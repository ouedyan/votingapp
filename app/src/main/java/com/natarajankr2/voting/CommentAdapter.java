package com.natarajankr2.voting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aghajari.emojiview.view.AXEmojiTextView;
import com.bumptech.glide.Glide;
import com.natarajankr2.voting.model.Comment;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Comment> comments;
    private boolean areReplies;
    private OnCommentAppreciationListener commentAppreciationListener = null;
    private OnReplyListener onReplyListener = null;
    private OnSeeCommentRepliesListener seeRepliesListener = null;

    public CommentAdapter(ArrayList<Comment> comments, boolean areReplies) {
        this.comments = comments;
        this.areReplies = areReplies;
    }

    public interface OnCommentAppreciationListener {
        void onAppreciation(Comment comment, Comment.CommentAppreciation appreciation, int positionInRecyclerView);
    }

    public interface OnReplyListener {
        void onReply(Comment replyToComment);
    }

    public interface OnSeeCommentRepliesListener {
        void onSeeReplies(Comment comment);
    }

    public void setOnReplyListener(OnReplyListener listener) {
        this.onReplyListener = listener;
    }

    public void setOnCommentAppreciationListener(OnCommentAppreciationListener listener) {
        this.commentAppreciationListener = listener;
    }

    public void setOnSeeCommentRepliesListener(OnSeeCommentRepliesListener listener) {
        this.seeRepliesListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_layout, parent, false);
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CommentViewHolder viewHolder = (CommentViewHolder) holder;
        Comment currentComment = comments.get(position);

        if (currentComment.getUser().getAvatarUrl() != null)
            Glide.with(viewHolder.itemView.getContext())
                    .load(currentComment.getUser().getAvatarUrl())
                    .placeholder(R.drawable.no_avatar)
                    .thumbnail(0.1f)
                    .into(viewHolder.avatar);

        viewHolder.username.setText(currentComment.getUser().getName());
        viewHolder.postTime.setText(new PrettyTime().format(currentComment.getPostDate()));
        viewHolder.commentText.setText(currentComment.getText());
        viewHolder.likesCount.setText(String.valueOf(currentComment.getLikesCount()));
        viewHolder.dislikesCount.setText(String.valueOf(currentComment.getDislikesCount()));
        viewHolder.seeRepliesText.setText(
                viewHolder.itemView.getResources().getQuantityString(
                        R.plurals.see_replies_text,
                        currentComment.getRepliesCount(),
                        currentComment.getRepliesCount()));

        if ((currentComment.getRepliesCount() == 0) || areReplies) {
            viewHolder.seeRepliesText.setVisibility(View.GONE);
            viewHolder.repliesArrow.setVisibility(View.GONE);
        } else {
            viewHolder.seeRepliesText.setVisibility(View.VISIBLE);
            viewHolder.repliesArrow.setVisibility(View.VISIBLE);
        }


        viewHolder.likeButton.setImageResource(R.drawable.ic_non_liked);
        viewHolder.dislikeButton.setImageResource(R.drawable.ic_non_disliked);
        if (currentComment.getLoggedInUserAppreciation() != null) {
            switch (currentComment.getLoggedInUserAppreciation()) {
                case LIKE:
                    viewHolder.likeButton.setImageResource(R.drawable.ic_liked);
                    break;
                case DISLIKE:
                    viewHolder.dislikeButton.setImageResource(R.drawable.ic_disliked);
                    break;
            }
        }

        viewHolder.likeButton.setOnClickListener(v ->
                this.commentAppreciationListener.onAppreciation(
                        currentComment,
                        Comment.CommentAppreciation.LIKE,
                        position));
        viewHolder.dislikeButton.setOnClickListener(v ->
                this.commentAppreciationListener.onAppreciation(
                        currentComment,
                        Comment.CommentAppreciation.DISLIKE,
                        position));

        viewHolder.replyButton.setOnClickListener(v -> this.onReplyListener.onReply(currentComment));

        View.OnClickListener seeRepliesClickListener = v -> this.seeRepliesListener.onSeeReplies(currentComment);
        viewHolder.seeRepliesText.setOnClickListener(seeRepliesClickListener);
        viewHolder.repliesArrow.setOnClickListener(seeRepliesClickListener);
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    static class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        ImageView repliesArrow;
        ImageView likeButton;
        ImageView dislikeButton;
        TextView username;
        TextView postTime;
        AXEmojiTextView commentText;
        TextView seeRepliesText;
        TextView likesCount;
        TextView dislikesCount;
        TextView replyButton;


        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            repliesArrow = itemView.findViewById(R.id.replies_arrow);
            likeButton = itemView.findViewById(R.id.like_ic);
            dislikeButton = itemView.findViewById(R.id.dislike_ic);
            username = itemView.findViewById(R.id.username);
            postTime = itemView.findViewById(R.id.post_time);
            commentText = itemView.findViewById(R.id.ax_comment_text);
            seeRepliesText = itemView.findViewById(R.id.see_replies_text);
            likesCount = itemView.findViewById(R.id.likes_count);
            dislikesCount = itemView.findViewById(R.id.dislikes_count);
            replyButton = itemView.findViewById(R.id.reply_text);

        }
    }
}
