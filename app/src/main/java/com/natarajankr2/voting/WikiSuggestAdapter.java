package com.natarajankr2.voting;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.natarajankr2.voting.model.WikiSuggest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WikiSuggestAdapter extends BaseAdapter implements Filterable {
    private static final String ENGLISH_WIKI_API_SEARCH_ENDPOINT_BASE_URL =
            "https://en.wikipedia.org/w/api.php?" +
                    "action=query" +
                    "&format=json" +
                    "&prop=description" +
                    "&generator=prefixsearch" +
                    "&redirects=1" +
                    "&converttitles=2" +
                    "&formatversion=2" +
                    "&gpslimit=5" +
                    "&gpssearch=";

    private Context mContext;
    private ArrayList<WikiSuggest> wikiSuggests = new ArrayList<>();
    private Filter filter = new WikiFilter();


    public WikiSuggestAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return wikiSuggests.size();
    }

    @Override
    public Object getItem(int position) {
        return wikiSuggests.get(position).getTitle();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout = convertView;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            layout = inflater.inflate(R.layout.wiki_suggest_layout, parent, false);
        }
        TextView title = layout.findViewById(R.id.title);
        TextView description = layout.findViewById(R.id.description);
        TextView redirectTitle = layout.findViewById(R.id.redirect_title);
        WikiSuggest currentSuggest = wikiSuggests.get(position);

        title.setText(currentSuggest.getTitle());

        if (currentSuggest.getDescription() != null && isRealDescription(currentSuggest.getDescription())) {
            description.setVisibility(View.VISIBLE);
            description.setText(currentSuggest.getDescription());
        } else
            description.setVisibility(View.GONE);

        if (currentSuggest.getRedirectTitle() != null){
            redirectTitle.setVisibility(View.VISIBLE);
            redirectTitle.setText(currentSuggest.getRedirectTitle());
        } else
            redirectTitle.setVisibility(View.GONE);

        return layout;
    }

    private boolean isRealDescription(String description){
        List<String> notRealDescriptions = Arrays.asList(
                "Disambiguation page providing links to topics that " +
                        "could be referred to by the same search term"
        );
        return !notRealDescriptions.contains(description) ;
    }

    private ArrayList<WikiSuggest> fetchWikiSuggests(String constraint) {
        ArrayList<WikiSuggest> fetchedWikiSuggests = new ArrayList<>();

        if (constraint != null && !constraint.isEmpty()) {

            ANRequest request = AndroidNetworking.get(ENGLISH_WIKI_API_SEARCH_ENDPOINT_BASE_URL + constraint)
                    .setPriority(Priority.HIGH)
                    .setTag("FETCH_WIKI_SUGGESTS")
                    .build();
            ANResponse<JSONObject> response = request.executeForJSONObject();
            if (response.isSuccess()) {
                JSONObject jsonResponse = response.getResult();
                Log.i("fetchWikiSuggests", jsonResponse.toString());

                if (jsonResponse.has("query")) {
                    try {
                        JSONObject queryNode = jsonResponse.getJSONObject("query");

                        JSONArray jsonResults = queryNode.getJSONArray("pages");
                        for (int i = 0; i < jsonResults.length(); i++) {
                            JSONObject currentJsonPage = jsonResults.getJSONObject(i);
                            WikiSuggest currentSuggest = new WikiSuggest(
                                    currentJsonPage.getInt("index"),
                                    currentJsonPage.getString("title"),
                                    currentJsonPage.has("description") ?
                                            currentJsonPage.getString("description") : null,
                                    null
                            );
                            fetchedWikiSuggests.add(currentSuggest);
                        }
                        //Sort the list by index
                        Collections.sort(fetchedWikiSuggests);

                        if (queryNode.has("redirects")) {
                            JSONArray jsonRedirects = queryNode.getJSONArray("redirects");
                            for (int i = 0; i < jsonRedirects.length(); i++) {
                                JSONObject currentRedirect = jsonRedirects.getJSONObject(i);
                                WikiSuggest concernedSuggest = fetchedWikiSuggests.get(currentRedirect.getInt("index") - 1);
                                concernedSuggest.setTitle(currentRedirect.getString("from"));
                                concernedSuggest.setRedirectTitle(currentRedirect.getString("to"));
                            }
                        }

                    } catch (Exception e) {
                        Log.e("fetchWikiSuggests", Objects.requireNonNull(e.getMessage()));
                    }

                }

            } else {
                ANError anError = response.getError();
                Log.e("fetchWikiSuggests", anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
            }
        }

        return fetchedWikiSuggests;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    private class WikiFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();

            if (constraint != null) {
                ArrayList<WikiSuggest> fetchedSuggests = fetchWikiSuggests(constraint.toString());
                // Assign the data to the FilterResults
                result.values = fetchedSuggests;
                result.count = fetchedSuggests.size();
            }

            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                wikiSuggests = (ArrayList<WikiSuggest>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
