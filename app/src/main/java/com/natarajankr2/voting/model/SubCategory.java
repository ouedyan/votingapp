package com.natarajankr2.voting.model;

import android.graphics.Bitmap;

public class SubCategory {
    private int mId;
    private Category mCategory ;
    private String mName;
    private Bitmap mImage;

    public int getId() {
        return mId;
    }

    public SubCategory setId(int id) {
        this.mId = id;
        return this;
    }

    public String getName() {
        return mName;
    }

    public SubCategory setName(String name) {
        this.mName = name;
        return this;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public SubCategory setImage(Bitmap image) {
        this.mImage = image;
        return this;
    }

    public Category getCategory() {
        return mCategory;
    }

    public SubCategory setCategory(Category category) {
        this.mCategory = category;
        return this;
    }
}
