package com.natarajankr2.voting.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class WikiSuggest implements Comparable<WikiSuggest>{
    private int index ;
    private String title;
    private String redirectTitle;
    private String description;

    public WikiSuggest(int index, @NonNull String title, @Nullable String description, @Nullable String redirectTitle){
        this.index = index ;
        this.title = title ;
        this.description = description ;
        this.redirectTitle = redirectTitle;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedirectTitle() {
        return redirectTitle;
    }

    public void setRedirectTitle(String redirectTitle) {
        this.redirectTitle = redirectTitle;
    }

    @Override
    public int compareTo(WikiSuggest o) {
        return Integer.compare(index, o.getIndex());
    }

}
