package com.natarajankr2.voting.model;


import com.natarajankr2.voting.ItemAdapter;
import com.natarajankr2.voting.ItemAdapter2;

public class Category extends ItemAdapter.ListItem implements ItemAdapter2.ListItem {
    private int mId;
    private String mName;
    private String mImageUrl;

    public int getId() {
        return mId;
    }

    public Category setId(int id) {
        this.mId = id;
        return this;
    }
    public String getName() {
        return mName;
    }

    public Category setName(String mName) {
        this.mName = mName;
        return this;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public Category setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
        return this;
    }

    @Override
    public int getListItemType() {
        return ItemAdapter2.ListItem.TYPE_CATEGORY;
    }

}
