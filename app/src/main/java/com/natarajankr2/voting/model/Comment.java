package com.natarajankr2.voting.model;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Comparable<Comment>, Serializable {
    private int id ;
    private int userId;
    private User user;
    private int itemId;
    private String text;
    private int replyToCommentId = -1;
    private Date postDate ;
    private int likesCount;
    private int dislikesCount;
    private int repliesCount;
    private CommentAppreciation loggedInUserAppreciation = null;

    public int getId() {
        return id;
    }

    public Comment setId(int id) {
        this.id = id;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Comment setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getItemId() {
        return itemId;
    }

    public Comment setItemId(int itemId) {
        this.itemId = itemId;
        return this;
    }

    public String getText() {
        return text;
    }

    public Comment setText(String text) {
        this.text = text;
        return this;
    }

    public int getReplyToCommentId() {
        return replyToCommentId;
    }

    public Comment setReplyToCommentId(int replyToCommentId) {
        this.replyToCommentId = replyToCommentId;
        return this;
    }

    public Date getPostDate() {
        return postDate;
    }

    public Comment setPostDate(Date postDate) {
        this.postDate = postDate;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Comment setUser(User user) {
        this.user = user;
        return this;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public Comment setLikesCount(int likesCount) {
        this.likesCount = likesCount;
        return this;
    }

    public int getDislikesCount() {
        return dislikesCount;
    }

    public Comment setDislikesCount(int dislikesCount) {
        this.dislikesCount = dislikesCount;
        return this;
    }

    public CommentAppreciation getLoggedInUserAppreciation() {
        return loggedInUserAppreciation;
    }

    public Comment setLoggedInUserAppreciation(CommentAppreciation loggedInUserAppreciation) {
        this.loggedInUserAppreciation = loggedInUserAppreciation;
        return this;
    }

    public int getRepliesCount() {
        return repliesCount;
    }

    public void setRepliesCount(int repliesCount) {
        this.repliesCount = repliesCount;
    }

    public enum CommentAppreciation {
        LIKE,
        DISLIKE
    }

    @Override
    public int compareTo(Comment o) {
        return this.postDate.compareTo(o.postDate);
    }
}
