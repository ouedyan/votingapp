package com.natarajankr2.voting.model;


import com.natarajankr2.voting.ItemAdapter;
import com.natarajankr2.voting.ItemAdapter2;

import java.io.Serializable;

public class Item extends ItemAdapter.ListItem implements ItemAdapter2.ListItem, Comparable<Item>, Serializable {
    private int mId;
    private int mCategoryId;
    private int mSubCategoryId;
    private int mTotalVotes = 0;
    private Option mOption1 = new Option();
    private Option mOption2 = new Option();
    private OptionChoice loggedUserVoted = null;

    public int getId() {
        return mId;
    }

    public Item setId(int id) {
        this.mId = id;
        return this;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public Item setCategoryId(int categoryId) {
        this.mCategoryId = categoryId;
        return this;
    }

    public int getSubCategoryId() {
        return mSubCategoryId;
    }

    public Item setSubCategoryId(int subCategoryId) {
        this.mSubCategoryId = subCategoryId;
        return this;
    }

    public int getTotalVotes() {
        return mTotalVotes;
    }

    public Item setTotalVotes(int mTotalVotes) {
        this.mTotalVotes = mTotalVotes;
        return this;
    }

    public String getOptionDescription(OptionChoice option) {
        switch (option) {
            case OPTION1:
                return this.mOption1.description;
            case OPTION2:
                return this.mOption2.description;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
    }

    public String getOptionImageUrl(OptionChoice option) {
        switch (option) {
            case OPTION1:
                return this.mOption1.imageUrl;
            case OPTION2:
                return this.mOption2.imageUrl;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
    }


    public Item setOptionDescription(OptionChoice option, String description) {
        switch (option) {
            case OPTION1:
                this.mOption1.description = description;
            case OPTION2:
                this.mOption2.description = description;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
        return this;
    }

    public OptionChoice getOptionLoggedInUserVoted() {
        return loggedUserVoted;
    }


    public boolean loggedUserVoted() {
        return loggedUserVoted != null;
    }

    public boolean loggedUserVoted(OptionChoice option) {
        return loggedUserVoted == option;
    }

    public Item setOptionLoggedUserVoted(OptionChoice option) {
        this.loggedUserVoted = option;
        return this;
    }

    public Item setOptionImageUrl(OptionChoice option, String imageUrl) {
        switch (option) {
            case OPTION1:
                this.mOption1.imageUrl = imageUrl;
                break;
            case OPTION2:
                this.mOption2.imageUrl = imageUrl;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
        return this;
    }

    public float getOptionVotesPercentage(OptionChoice option) {
        switch (option) {
            case OPTION1:
                return this.mOption1.votePercentage;
            case OPTION2:
                return this.mOption2.votePercentage;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
    }


    public Item setOptionVotePercentage(OptionChoice option, float votePercentage) {
        switch (option) {
            case OPTION1:
                this.mOption1.votePercentage = votePercentage;
                break;
            case OPTION2:
                this.mOption2.votePercentage = votePercentage;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
        return this;
    }
    
    public int getOptionVotesCount(OptionChoice option) {
        switch (option) {
            case OPTION1:
                return mOption1.votesCount;
            case OPTION2:
                return mOption2.votesCount;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
    }

    public void recomputeOptionVotePercentages() {
        mOption1.votePercentage = (float)mOption1.votesCount / (float)this.mTotalVotes * 100;
        mOption2.votePercentage = (float)mOption2.votesCount / (float)this.mTotalVotes * 100;
    }



    public Item setOptionVotesCount(OptionChoice option, int votesCount) {
        switch (option) {
            case OPTION1:
                this.mOption1.votesCount = votesCount;
                break;
            case OPTION2:
                this.mOption2.votesCount = votesCount;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }
        return this;
    }

    public OptionChoice getMostVotedOption() {
        return mOption1.votesCount > this.mOption2.votesCount ? OptionChoice.OPTION1 : OptionChoice.OPTION2;
    }

    public OptionChoice getLessVotedOption() {
        return mOption2.votesCount < this.mOption1.votesCount ? OptionChoice.OPTION2 : OptionChoice.OPTION1;
    }


    @Override
    public int getListItemType() {
        return ItemAdapter2.ListItem.TYPE_ITEM;
    }

    @Override
    public int compareTo(Item item) {
        return Integer.compare(this.mTotalVotes, item.mTotalVotes);
    }


    public enum OptionChoice {
        OPTION1,
        OPTION2
    }


    public static class Option implements Serializable {
        String description;
        String imageUrl;
        float votePercentage = 0f;
        int votesCount;
    }
}
