package com.natarajankr2.voting.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;


public class CategoryConstraintLayout extends ConstraintLayout{
    public CategoryConstraintLayout(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void draw(Canvas canvas) {
        Path path = getPath() ;
        /*Paint paint = new Paint();
        paint.setColor(Color.parseColor("#000000"));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        canvas.drawPath(path,paint);*/
        canvas.clipPath(path) ;
        super.draw(canvas);
    }

    private Path getPath(){
        Path path = new Path();
        path.setFillType(Path.FillType.WINDING);
        path.moveTo(0, (float) (0.3 * getHeight()));
        path.lineTo(0,getHeight());
        path.lineTo(getWidth(), (float) (0.85 * getHeight()));
        path.lineTo(getWidth(),(float) (0.15 * getHeight()));
        path.close();
        return path;
    }

}
