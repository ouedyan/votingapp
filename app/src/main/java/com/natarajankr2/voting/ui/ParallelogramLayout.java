package com.natarajankr2.voting.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GestureDetectorCompat;

import com.natarajankr2.voting.R;

public class ParallelogramLayout extends ConstraintLayout {
    private GestureDetectorCompat gestureDetector;
    private boolean isSingleTapConfirmed = true;
    private MotionEvent lastActionDownEvent = null;
    private Utils.OnDoubleClickListener onDoubleClickListener = null;
    private int oppositeLengthVerticalOffset = 0;
    private int parallelogramHeight = -1;


    public ParallelogramLayout(Context context) {
        this(context, null, 0, 0);
    }

    public ParallelogramLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public ParallelogramLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ParallelogramLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        //Default parallelogramHeight
        /*post(() -> {
            parallelogramHeight = getHeight() - oppositeLengthVerticalOffset;
            invalidate();
        });*/

        oppositeLengthVerticalOffset = 0;

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ParallelogramLayout, defStyleAttr, defStyleRes);

            try {
                setParallelogramHeight(a.getDimensionPixelSize(R.styleable.ParallelogramLayout_parallelogramHeight, -1));
                setOppositeLengthVerticalOffset(a.getDimensionPixelSize(R.styleable.ParallelogramLayout_oppositeLengthVerticalOffset, 0));
            } finally {
                a.recycle();
            }
        }

        GestureDetector.OnDoubleTapListener doubleTapListener = new GestureDetector.OnDoubleTapListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                performClick();
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (ParallelogramLayout.this.onDoubleClickListener != null)
                    ParallelogramLayout.this.onDoubleClickListener.onDoubleClick();
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return false;
            }
        };
        GestureDetectorCompat gestureDetectorCompat = new GestureDetectorCompat(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        gestureDetector = gestureDetectorCompat;
        gestureDetectorCompat.setOnDoubleTapListener(doubleTapListener);
    }

    private Path getPath() {
        Path path = new Path();
        path.setFillType(Path.FillType.WINDING);
        int realParallelogramHeight = parallelogramHeight == -1 ?
                getHeight() - Math.abs(oppositeLengthVerticalOffset) : parallelogramHeight;
        if (oppositeLengthVerticalOffset > 0) {
            path.moveTo(0, (float) (getHeight() - realParallelogramHeight));
            path.lineTo(0, (float) getHeight());
            path.lineTo((float) getWidth(), (float) (getHeight() - oppositeLengthVerticalOffset));
            path.lineTo((float) getWidth(), (float) (getHeight() - oppositeLengthVerticalOffset - realParallelogramHeight));
        } else {
            path.moveTo(0, 0);
            path.lineTo(0, (float) realParallelogramHeight);
            path.lineTo((float) getWidth(), (float) (-oppositeLengthVerticalOffset + realParallelogramHeight));
            path.lineTo((float) getWidth(), (float) (-oppositeLengthVerticalOffset));
        }

        path.close();
        return path;
    }

    @Override
    public void draw(Canvas canvas) {
        if (parallelogramHeight != 0) {
            canvas.clipPath(getPath());
        }

        super.draw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (parallelogramHeight != 0) {
            canvas.clipPath(getPath());
        }

        super.dispatchDraw(canvas);
    }

    public int getOppositeLengthVerticalOffset() {
        return oppositeLengthVerticalOffset;
    }

    public int getParallelogramHeight() {
        return parallelogramHeight;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            lastActionDownEvent = MotionEvent.obtain(event);
        }

        Bitmap viewBitmap = Utils.getViewAsBitmap(this);
        boolean pixelIsTransparent =
                viewBitmap.getPixel((int) lastActionDownEvent.getX(), (int) lastActionDownEvent.getY()) == Color.TRANSPARENT;

        if (pixelIsTransparent) {
            return false;
        } else {
            //gestureDetector.onTouchEvent(event);
            return super.onTouchEvent(event);
        }
    }

    public Utils.OnDoubleClickListener getOnDoubleClickListener() {
        return onDoubleClickListener;
    }


    public void setOnDoubleClickListener(Utils.OnDoubleClickListener onDoubleClickListener) {
        onDoubleClickListener = onDoubleClickListener;
    }

    /*public void setOppositeLengthVerticalOffset(int offset) {
        post(() -> {
            // +1 as rounding error incertitude at #getDimensionInPixels()
            if (parallelogramHeight == -1) {
                if (Math.abs(offset) <= getHeight() + 1) {
                    oppositeLengthVerticalOffset = offset;
                    invalidate();
                } else {
                    throw new ArithmeticException("oppositeLengthVerticalOffset's absolute value can't be greater than " +
                            "this View's height : |" + offset + "| > " + getHeight());
                }
            }
            // +1 as rounding error incertitude at #getDimensionInPixels()
            else if (Math.abs(offset) <= getHeight() - parallelogramHeight + 1) {
                oppositeLengthVerticalOffset = offset;
                invalidate();
            } else {
                throw new ArithmeticException("oppositeLengthVerticalOffset's absolute value can't be greater than the difference of" +
                        "this View's height and the parallelogram's height : |" + offset + "| > " + getHeight() + " - " +
                        parallelogramHeight);
            }
        });
    }*/

    public void setOppositeLengthVerticalOffset(int offset) {
        post(() -> {
            oppositeLengthVerticalOffset = offset;
            invalidate();
        });
    }


    public void setParallelogramHeight(int height) {
        post(() -> {
            /*Log.e("setParallelogramHeight", this.toString() + "\n" + "height = " + height + "\n" + "getHeight() = " +
                    getHeight());*/
            // +1 as rounding error incertitude at #getDimensionInPixels()
            if (height <= getHeight() + 1) {
                if (height == -1) {
                    parallelogramHeight = height;
                    invalidate();
                } else if (height >= 0) {
                    parallelogramHeight = height;
                    invalidate();
                } else {
                    throw new ArithmeticException("The parallelogram's height can't be negative: " + height);
                }
            } else {
                throw new ArithmeticException("The parallelogram's height can't be greater than this View's height: " +
                        height + " > " + getHeight());
            }
        });
    }
}
