package com.natarajankr2.voting.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.natarajankr2.voting.Auth;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class CompleteRegistrationActivity extends AppCompatActivity {
    private ImageView avatarImage;
    private TextInputEditText editFirstName;
    private TextInputEditText editLastName;
    private View loadingOverlay;
    private ProgressBar progressBar;
    private String email;
    private String firstName;
    private String lastName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_registration);

        avatarImage = findViewById(R.id.avatar);

        loadingOverlay = findViewById(R.id.loading_overlay);
        progressBar = findViewById(R.id.progress);

        editFirstName = findViewById(R.id.edit_first_name);
        editLastName = findViewById(R.id.edit_last_name);
        TextInputEditText editEmail = findViewById(R.id.edit_Email);
        Button buttonRegister = findViewById(R.id.button_register);

        progressBar.setVisibility(View.GONE);

        editEmail.setText(Auth.loggedInUser.getEmail());
        editEmail.setEnabled(false);

        OnEditorActionListener onEditorActionListener = (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) finishSignUp();
            return false;
        };
        editLastName.setOnEditorActionListener(onEditorActionListener);

        buttonRegister.setOnClickListener(v -> finishSignUp());
    }

    private void finishSignUp() {
        if (!validateFields()) return;

        tryCompleteRegistration();
    }

    private void clearErrors() {
        editFirstName.setError(null);
        editLastName.setError(null);
    }

    private void tryCompleteRegistration() {
        loadingOverlay.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        String name = firstName + " " + lastName;
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.COMPLETE_REGISTRATION)
                .addBodyParameter("userId", String.valueOf(Auth.loggedInUser.getId()))
                .addBodyParameter("avatarUrl", String.valueOf(ConnectToDatabase.AVATAR_NULL))
                .addBodyParameter("name", name)
                .setTag("COMPLETE_REGISTRATION")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("tryCompleteRegistration",response.toString());
                        try {
                            if (response.getInt("result") == ConnectToDatabase.RESULT_SUCCESS) {
                                Toast.makeText(CompleteRegistrationActivity.this, "Successfully registered", Toast.LENGTH_SHORT).show();
                                //setResult(RESULT_OK);
                                Auth.loggedInUser.setAvatarUrl(null);
                                Auth.loggedInUser.setName(name);
                                Auth.loggedInUser.setPhone(null);
                                startActivity(new Intent(CompleteRegistrationActivity.this, MainActivity.class));
                                finish();
                            } else {//Error
                                Log.e("tryCompleteRegistration", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(CompleteRegistrationActivity.this, "Error while registering",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("tryCompleteRegistration", Objects.requireNonNull(e.getMessage()));
                            e.printStackTrace();
                            findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(CompleteRegistrationActivity.this, "Error while registering",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("tryCompleteRegistration",
                                anError.getErrorDetail() + ": " + anError.getMessage());
                        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CompleteRegistrationActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }


    private boolean validateFields() {
        boolean result = true;

        firstName = Objects.requireNonNull(editFirstName.getText()).toString().trim();
        lastName = Objects.requireNonNull(editLastName.getText()).toString().trim();
        if (TextUtils.isEmpty(firstName)) {
            editFirstName.setError("Please enter your first name");
            result = false;
        }

        if (TextUtils.isEmpty(lastName)) {
            editLastName.setError("Please enter your last name");
            result = false;
        }

        return result;
    }
}