package com.natarajankr2.voting.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aghajari.emojiview.AXEmojiManager;
import com.aghajari.emojiview.emoji.iosprovider.AXIOSEmojiProvider;
import com.aghajari.emojiview.listener.SimplePopupAdapter;
import com.aghajari.emojiview.search.AXEmojiSearchView;
import com.aghajari.emojiview.view.AXEmojiEditText;
import com.aghajari.emojiview.view.AXEmojiPager;
import com.aghajari.emojiview.view.AXEmojiPopupLayout;
import com.aghajari.emojiview.view.AXEmojiTextView;
import com.aghajari.emojiview.view.AXEmojiView;
import com.aghajari.emojiview.view.AXStickerView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.natarajankr2.voting.CommentAdapter;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.model.Comment;
import com.natarajankr2.voting.model.User;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

import static com.natarajankr2.voting.ConnectToDatabase.RESULT_SUCCESS;

public class CommentRepliesActivity extends AppCompatActivity {
    private static final int NOT_IN_RECYCLERVIEW = -1;
    private final ArrayList<Comment> commentReplies = new ArrayList<>();
    private Comment comment;
    private User loggedInUser;
    private boolean isEmojiShowing = false;

    private boolean intentIsReplying;

    private AXEmojiEditText emojiEditText;
    private AXEmojiPopupLayout emojiPopupLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private View rootView;

    private ImageView avatar;
    private ImageView likeButton;
    private ImageView dislikeButton;
    private TextView username;
    private TextView postTime;
    private AXEmojiTextView commentText;
    private TextView likesCount;
    private TextView dislikesCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_replies);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        AXEmojiManager.install(this, new AXIOSEmojiProvider(this));

        loggedInUser = (User) getIntent().getSerializableExtra("loggedInUser");
        comment = (Comment) getIntent().getSerializableExtra("comment");
        intentIsReplying = getIntent().getBooleanExtra("replying", false);

        rootView = findViewById(R.id.root_view);
        recyclerView = findViewById(R.id.recyclerview);
        swipeRefreshLayout = findViewById(R.id.smart_refresh);
        emojiEditText = findViewById(R.id.emojiEdit);
        emojiPopupLayout = findViewById(R.id.emojiPopupLayout);

        avatar = findViewById(R.id.avatar);
        likeButton = findViewById(R.id.like_ic);
        dislikeButton = findViewById(R.id.dislike_ic);
        username = findViewById(R.id.username);
        postTime = findViewById(R.id.post_time);
        commentText = findViewById(R.id.ax_comment_text);
        likesCount = findViewById(R.id.likes_count);
        dislikesCount = findViewById(R.id.dislikes_count);


        if (comment.getUser().getAvatarUrl() != null)
            Glide.with(this)
                    .load(comment.getUser().getAvatarUrl())
                    .placeholder(R.drawable.no_avatar)
                    .thumbnail(0.1f)
                    .into(avatar);

        username.setText(comment.getUser().getName());
        postTime.setText(new PrettyTime().format(comment.getPostDate()));
        commentText.setText(comment.getText());
        updateMainCommentAppreciation();


        likeButton.setOnClickListener(v -> {
            appreciateComment(comment.getId(), loggedInUser.getId(), Comment.CommentAppreciation.LIKE, NOT_IN_RECYCLERVIEW);
        });
        dislikeButton.setOnClickListener(v -> {
            appreciateComment(comment.getId(), loggedInUser.getId(), Comment.CommentAppreciation.DISLIKE,
                    NOT_IN_RECYCLERVIEW);
        });

        //Recycler view click listeners

        CommentAdapter commentAdapter = new CommentAdapter(commentReplies, true);
        commentAdapter.setOnCommentAppreciationListener((comment, appreciation, positionInRecyclerView) -> {
            appreciateComment(comment.getId(), loggedInUser.getId(), appreciation, positionInRecyclerView);
        });

        recyclerView.setAdapter(commentAdapter);
        new LoadDataFromDatabase(this).execute();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            new LoadDataFromDatabase(CommentRepliesActivity.this, commentReplies.isEmpty()).execute();
        });

        //Emoji, stickers, GIFs configuration
        AXEmojiView emojiView = new AXEmojiView(this);
        AXStickerView stickerView = new AXStickerView(this, "stickers", new CustomStickerProvider());
        EmojiGIFPage gifPage = new EmojiGIFPage(this);
        AXEmojiPager emojiPager = new AXEmojiPager(this);
        emojiPager.addPage(emojiView, R.drawable.ic_msg_panel_smiles);
        emojiPager.addPage(stickerView, R.drawable.ic_msg_panel_stickers);
        emojiPager.addPage(gifPage, R.drawable.ic_gif_panel);
        emojiPager.setEditText(emojiEditText);
        emojiPager.setSwipeWithFingerEnabled(true);
        emojiPager.setLeftIcon(R.drawable.ic_ab_search);
        /*emojiPager.setOnFooterItemClicked((view, leftIcon) -> {
            if (leftIcon) Toast.makeText(CommentRepliesActivity.this,"Search Clicked", Toast.LENGTH_SHORT).show();
        });*/
        AXEmojiManager.getEmojiViewTheme().setFooterEnabled(true);

        // create emoji popup
        emojiPopupLayout.initPopupView(emojiPager);

        // Emoji SearchView
        if (AXEmojiManager.isAXEmojiView(emojiPager.getPage(0))) {
            emojiPopupLayout.setSearchView(new AXEmojiSearchView(this, emojiPager.getPage(0)));
            emojiPager.setOnFooterItemClicked((view, leftIcon) -> {
                if (leftIcon) emojiPopupLayout.showSearchView();
            });
        }

        emojiEditText.setOnClickListener(view -> {
            emojiPopupLayout.openKeyboard();
            if (commentReplies.size() - 1 > 0)
                recyclerView.smoothScrollToPosition(commentReplies.size() - 1);
        });

        ImageView switch_kbd_emoji = findViewById(R.id.switch_kbd_emoji);
        switch_kbd_emoji.setOnClickListener(view -> {
            if (isEmojiShowing) {
                emojiPopupLayout.openKeyboard();
            } else {
                emojiPopupLayout.show();
            }
        });

        //Send Action
        findViewById(R.id.send).setOnClickListener(view -> {
            if (Objects.requireNonNull(emojiEditText.getText()).length() > 0) {
                emojiPopupLayout.dismiss();
                addComment(loggedInUser.getId(), comment.getItemId(), emojiEditText.getText().toString());
            }
        });

        emojiPopupLayout.setPopupListener(new SimplePopupAdapter() {
            @Override
            public void onShow() {
                updateButton(true);
            }

            @Override
            public void onDismiss() {
                updateButton(false);
            }

            @Override
            public void onKeyboardOpened(int height) {
                updateButton(false);
            }

            @Override
            public void onKeyboardClosed() {
                updateButton(emojiPopupLayout.isShowing());
            }

            private void updateButton(boolean emoji) {
                if (isEmojiShowing == emoji) return;
                isEmojiShowing = emoji;
                if (emoji) {
                    switch_kbd_emoji.setImageResource(R.drawable.ic_msg_panel_kb);
                } else {
                    switch_kbd_emoji.setImageResource(R.drawable.ic_msg_panel_smiles);
                }
            }

        });


        commentAdapter.setOnReplyListener(replyToComment -> { //Reply with user name as tag
            //TODO
            emojiEditText.setText("@" + replyToComment.getUser().getName());
            emojiEditText.requestFocus();
        });


        if(intentIsReplying) //focus on editText
            emojiEditText.requestFocus();
    }

    private static class LoadDataFromDatabase extends AsyncTask<Void, Integer, ArrayList<Comment>> {
        WeakReference<CommentRepliesActivity> activityWeakReference;
        boolean recyclerViewEmpty = true;
        ArrayList<Comment> commentReplies;
        Comment comment;

        LoadDataFromDatabase(CommentRepliesActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
            commentReplies = activity.commentReplies;
            comment = activity.comment;

        }

        LoadDataFromDatabase(CommentRepliesActivity activity, boolean isRecyclerViewEmpty) {
            activityWeakReference = new WeakReference<>(activity);
            recyclerViewEmpty = isRecyclerViewEmpty;
            commentReplies = activity.commentReplies;
            comment = activity.comment;
        }

        @Override
        protected void onPreExecute() {
            CommentRepliesActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.setLoading(recyclerViewEmpty);
        }

        @Override
        protected ArrayList<Comment> doInBackground(Void... params) {
            CommentRepliesActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return null;
            try {
                ArrayList<Comment> allCommentReplies = ConnectToDatabase.getAllCommentReplies(comment.getId());
                commentReplies.clear();
                for (Comment comment : allCommentReplies) {
                    comment.setLikesCount(ConnectToDatabase.getCommentAppreciationTotalCount(comment.getId(), Comment.CommentAppreciation.LIKE));
                    comment.setDislikesCount(ConnectToDatabase.getCommentAppreciationTotalCount(comment.getId(), Comment.CommentAppreciation.DISLIKE));
                    comment.setUser(ConnectToDatabase.getUser(comment.getUserId()));
                    if (ConnectToDatabase.checkUserAppreciatedComment(
                            activity.loggedInUser.getId(),
                            comment.getId(),
                            Comment.CommentAppreciation.LIKE))
                        comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.LIKE);
                    if (ConnectToDatabase.checkUserAppreciatedComment(
                            activity.loggedInUser.getId(),
                            comment.getId(),
                            Comment.CommentAppreciation.DISLIKE))
                        comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.DISLIKE);
                    comment.setRepliesCount(ConnectToDatabase.getCommentRepliesCount(comment.getId()));
                    commentReplies.add(comment);
                }
                return commentReplies;

            } catch (Exception e) {
                Log.e("LoadDataFromDatabase", e.toString(), e);
                return null;
            }
        }


        @Override
        protected void onPostExecute(ArrayList<Comment> result) {
            CommentRepliesActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.stopLoading();

            if (result != null) {
                Objects.requireNonNull(activity.recyclerView.getAdapter()).notifyDataSetChanged();
                if (commentReplies.isEmpty())
                    activity.findViewById(R.id.no_comment_text).setVisibility(View.VISIBLE);
            } else {
                ImageView connectionErrorImage = activity.findViewById(R.id.connection_error_image);
                if (recyclerViewEmpty) {
                    connectionErrorImage.setVisibility(View.VISIBLE);
                }
                Toast.makeText(activity, "Error while getting data from database. Swipe to retry",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void appreciateComment(int commentId, int userId, Comment.CommentAppreciation commentAppreciation,
                                   int positionInRecyclerView) {
        if (positionInRecyclerView != NOT_IN_RECYCLERVIEW)
            setLoading(commentReplies.isEmpty());
        else {
            findViewById(R.id.loading_overlay).setVisibility(View.VISIBLE);//for swipe to refresh
            swipeRefreshLayout.setRefreshing(true);
        }
        int appreciation = (commentAppreciation == Comment.CommentAppreciation.DISLIKE ? -1 : 1);
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.APPRECIATE_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("commentId", String.valueOf(commentId))
                .addBodyParameter("appreciation", String.valueOf(appreciation))
                .setPriority(Priority.HIGH)
                .setTag("APPRECIATE_COMMENT")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("appreciateComment", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                stopLoading();
                                if (response.getBoolean("content")) {// Successful

                                    Comment comment = positionInRecyclerView != NOT_IN_RECYCLERVIEW ?
                                            commentReplies.get(positionInRecyclerView) :
                                            CommentRepliesActivity.this.comment ;
                                    if (commentAppreciation == Comment.CommentAppreciation.LIKE) {
                                        if (comment.getLoggedInUserAppreciation() != commentAppreciation) {
                                            if (comment.getLoggedInUserAppreciation() != null) // User already disliked before
                                                comment.setDislikesCount(comment.getDislikesCount() - 1);

                                            comment.setLikesCount(comment.getLikesCount() + 1);
                                            comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.LIKE);
                                        } else {
                                            comment.setLikesCount(comment.getLikesCount() - 1);
                                            comment.setLoggedInUserAppreciation(null);
                                        }

                                    } else { //Dislike
                                        if (comment.getLoggedInUserAppreciation() != commentAppreciation) {
                                            if (comment.getLoggedInUserAppreciation() != null)// User already liked before
                                                comment.setLikesCount(comment.getLikesCount() - 1);

                                            comment.setDislikesCount(comment.getDislikesCount() + 1);
                                            comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.DISLIKE);

                                        } else {
                                            comment.setDislikesCount(comment.getDislikesCount() - 1);
                                            comment.setLoggedInUserAppreciation(null);
                                        }
                                    }

                                    if (positionInRecyclerView != NOT_IN_RECYCLERVIEW)
                                        Objects.requireNonNull(recyclerView.getAdapter()).notifyItemChanged(positionInRecyclerView);
                                    else  //Main comment
                                        updateMainCommentAppreciation();

                                }
                            } else {//Error
                                stopLoading();
                                Log.e("appreciateComment : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                Toast.makeText(CommentRepliesActivity.this, "An error occurred",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            stopLoading();
                            Log.e("appreciateComment", Objects.requireNonNull(e.getMessage()));
                            Toast.makeText(CommentRepliesActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        stopLoading();
                        Log.e("appreciateComment",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        Toast.makeText(CommentRepliesActivity.this, "Can't reach database", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void updateMainCommentAppreciation() {
        likesCount.setText(String.valueOf(comment.getLikesCount()));
        dislikesCount.setText(String.valueOf(comment.getDislikesCount()));

        likeButton.setImageResource(R.drawable.ic_non_liked);
        dislikeButton.setImageResource(R.drawable.ic_non_disliked);
        if (comment.getLoggedInUserAppreciation() != null) {
            switch (comment.getLoggedInUserAppreciation()) {
                case LIKE:
                    likeButton.setImageResource(R.drawable.ic_liked);
                    break;
                case DISLIKE:
                    dislikeButton.setImageResource(R.drawable.ic_disliked);
                    break;
            }
        }
    }


    private void addComment(int userId, int itemId, String text) {
        setLoading(commentReplies.isEmpty());
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.ADD_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("itemId", String.valueOf(itemId))
                .addBodyParameter("text", String.valueOf(text))
                .addBodyParameter("replyToCommentId", String.valueOf(comment.getId()))
                .setPriority(Priority.HIGH)
                .setTag("ADD_COMMENT")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("addComment", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                emojiEditText.setText("");
                                new CommentRepliesActivity.LoadDataFromDatabase(CommentRepliesActivity.this, commentReplies.isEmpty()).execute();

                            } else {//Error
                                stopLoading();
                                Log.e("addComment : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                Toast.makeText(CommentRepliesActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            stopLoading();
                            Log.e("addComment", Objects.requireNonNull(e.getMessage()));
                            Toast.makeText(CommentRepliesActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        stopLoading();
                        Log.e("addComment",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        Toast.makeText(CommentRepliesActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void setLoading(boolean recyclerViewEmpty) {
        findViewById(R.id.no_comment_text).setVisibility(View.GONE);
        findViewById(R.id.loading_overlay).setVisibility(View.VISIBLE);//for swipe to refresh
        findViewById(R.id.connection_error_image).setVisibility(View.GONE);
        if (recyclerViewEmpty)
            findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onBackPressed() {
        if (!emojiPopupLayout.onBackPressed())
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem comment) {
        if (comment.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(comment);
    }
}