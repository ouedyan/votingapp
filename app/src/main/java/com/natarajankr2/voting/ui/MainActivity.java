package com.natarajankr2.voting.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.natarajankr2.voting.Auth;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.ItemAdapter2;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.model.Category;
import com.natarajankr2.voting.model.Item;
import com.natarajankr2.voting.model.User;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


import static android.view.View.VISIBLE;
import static com.natarajankr2.voting.ConnectToDatabase.RESULT_SUCCESS;

public class MainActivity extends AppCompatActivity {
    //private static final int LOGIN_REQUEST = 1;
    //private static final int COMPLETE_REGISTRATION_REQUEST = 2;
    private static final int NEW_BATTLE_REQUEST = 3;
    private static final int NEW_BATTLE_RESULT_CONNECTION_ERROR = 4;

    private User loggedInUser = null;

    private final ArrayList<ItemAdapter2.ListItem> listItems = new ArrayList<>();
    private RecyclerView recyclerView;
    private SmartRefreshLayout smartRefreshLayout;
    private ItemAdapter2 itemAdapter;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidNetworking.initialize(getApplicationContext());

        //ensureUserLoggedIn();
        loggedInUser = Auth.loggedInUser ;

        setContentView(R.layout.activity_main);
        rootView = findViewById(R.id.root_view);

        setSupportActionBar(findViewById(R.id.my_toolbar));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        recyclerView = findViewById(R.id.recyclerview);
        smartRefreshLayout = findViewById(R.id.smart_refresh);

        itemAdapter = new ItemAdapter2(listItems, (LinearLayoutManager) recyclerView.getLayoutManager()); //In order to not
        // override already set layout manager properties (in ItemAdapter)

        //Vote handling
        itemAdapter.setOnItemOptionClickListener((optionView, item, option, itemView, positionInList)
                -> voteItemOption(item, option, loggedInUser.getId(), optionView.getId(), itemView, positionInList)
        );

        itemAdapter.setOnGoToCommentsClick(item -> {
            Intent intent = new Intent(MainActivity.this, ViewItemActivity.class);
            intent.putExtra("user", loggedInUser);
            intent.putExtra("item", item);
            startActivity(intent);
        });

        recyclerView.setAdapter(itemAdapter);

        smartRefreshLayout.setOnRefreshListener(refreshLayout -> new LoadDataFromDatabase(MainActivity.this, false).execute());

        new LoadDataFromDatabase(MainActivity.this).execute();
    }

    /*private void ensureUserLoggedIn() {
        if (Auth.loggedInUser == null)
            startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
    }*/

    public void newBattle(View view) {
        startActivityForResult(new Intent(this, NewBattleActivity.class), NEW_BATTLE_REQUEST);
    }


    private static class LoadDataFromDatabase extends AsyncTask<Void, ItemAdapter2.ListItem, ArrayList<ItemAdapter2.ListItem>> {
        WeakReference<MainActivity> activityWeakReference;
        boolean recyclerViewEmpty = true;
        ArrayList<ItemAdapter2.ListItem> listItems;
        User loggedInUser;
        int previousListItemsSize;

        LoadDataFromDatabase(MainActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
            listItems = activity.listItems;
            loggedInUser = activity.loggedInUser;
            previousListItemsSize = listItems.size();

        }

        LoadDataFromDatabase(MainActivity activity, boolean isRecyclerViewEmpty) {
            activityWeakReference = new WeakReference<>(activity);
            recyclerViewEmpty = isRecyclerViewEmpty;
            listItems = activity.listItems;
            loggedInUser = activity.loggedInUser;
            previousListItemsSize = listItems.size();
        }

        @Override
        protected void onPreExecute() {
            MainActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.setLoading(recyclerViewEmpty);
        }

        @Override
        protected ArrayList<ItemAdapter2.ListItem> doInBackground(Void... params) {
            try {
                ArrayList<Category> allCategories = ConnectToDatabase.getAllCategories();
                ArrayList<Item> allItems = ConnectToDatabase.getAllItems();
                listItems.clear();
                for (Category category : allCategories) {
                    publishProgress(category);
                    for (Item item : allItems) {
                        if (item.getCategoryId() == category.getId()) {
                            //TODO Optimize Come from server in ConnectDatabase SHIT !!
                            HashMap<Item.OptionChoice, Integer> itemVotesCount =
                                    ConnectToDatabase.getItemOptionsVotesCount(item.getId());
                            item.setOptionVotesCount(Item.OptionChoice.OPTION1, Objects.requireNonNull(itemVotesCount.get(Item.OptionChoice.OPTION1)));
                            item.setOptionVotesCount(Item.OptionChoice.OPTION2, Objects.requireNonNull(itemVotesCount.get(Item.OptionChoice.OPTION2)));
                            item.recomputeOptionVotePercentages();

                            //TODO Optimize Come from server in ConnectDatabase SHIT !!
                            if (ConnectToDatabase.checkUserVotedItemOption(loggedInUser.getId(), item.getId(), Item.OptionChoice.OPTION1))
                                item.setOptionLoggedUserVoted(Item.OptionChoice.OPTION1);
                                //TODO Optimize Come from server in ConnectDatabase SHIT !!
                            else if (ConnectToDatabase.checkUserVotedItemOption(loggedInUser.getId(), item.getId(), Item.OptionChoice.OPTION2))
                                item.setOptionLoggedUserVoted(Item.OptionChoice.OPTION2);

                            publishProgress(item);
                        }
                    }
                }
                return listItems;

            } catch (Exception e) {
                Log.e("LoadDataFromDatabase", e.toString(), e);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(ItemAdapter2.ListItem... listItems) {
            MainActivity activity = activityWeakReference.get();
            if (activity != null && !activity.isFinishing()) {
                ItemAdapter2.ListItem listItem = listItems[0];
                this.listItems.add(listItem);
                if (activity.isLoading()) {
                    activity.stopLoading();
                }

                if (this.listItems.size() == 1) {
                    activity.itemAdapter.notifyDataSetChanged();
                } else {
                    activity.itemAdapter.notifyItemInserted(this.listItems.size() - 1);
                }
            }

        }

        @Override
        protected void onPostExecute(ArrayList<ItemAdapter2.ListItem> result) {
            MainActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.stopLoading();

            if (result == null) {
                ImageView connectionErrorImage = activity.findViewById(R.id.connection_error_image);
                if (recyclerViewEmpty) {
                    connectionErrorImage.setVisibility(VISIBLE);
                }
                Snackbar.make(activity.rootView, "Error while getting data from database.",
                        BaseTransientBottomBar.LENGTH_LONG)
                        .setAction("Retry", v -> {
                            new LoadDataFromDatabase(activity, recyclerViewEmpty).execute();
                        })
                        .show();
            }
        }

    }


    private void voteItemOption(Item item, Item.OptionChoice option, int userId, int optionViewId, View itemView,
                                int positionInList) {
        setLoading(false);
        int optionVotedNumber = (option == Item.OptionChoice.OPTION1 ? 1 : 2);
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.VOTE_ITEM_OPTION)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("itemId", String.valueOf(item.getId()))
                .addBodyParameter("optionVotedNumber", String.valueOf(optionVotedNumber))
                .setPriority(Priority.HIGH)
                .setTag("VOTE_ITEM_OPTION")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("voteItemOption", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                stopLoading();
                                boolean result = response.getBoolean("content");
                                if (result) {// Vote successful
                                    boolean cancellation = item.loggedUserVoted(option); //Check in old item
                                    String toastText = cancellation ? "Vote cancelled" : "Hurray ! One vote added";
                                    //showToastForDuration(activity.getResources().getInteger(R.integer
                                    // .successful_vote_toast_seconds_duration) * 1000, toastText, activity);
                                    Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_LONG).show();

                                    if (!cancellation) {
                                        item.setOptionLoggedUserVoted(option);
                                        item.setTotalVotes(item.getTotalVotes() + 1);
                                        item.setOptionVotesCount(option, item.getOptionVotesCount(option) + 1);

                                    } else {
                                        item.setOptionLoggedUserVoted(null);
                                        item.setTotalVotes(item.getTotalVotes() - 1);
                                        item.setOptionVotesCount(option, item.getOptionVotesCount(option) - 1);
                                    }
                                    item.recomputeOptionVotePercentages();

                                    HashMap<String, HashMap<String, Object>> mapsOfPayloads = new HashMap<>();
                                    HashMap<String, Object> optionVotedPayload = new HashMap<>();
                                    optionVotedPayload.put("concernedOption", option);
                                    //optionVotedPayload.put("cancellation", cancellation);
                                    mapsOfPayloads.put("optionVotedPayload", optionVotedPayload);
                                    itemAdapter.notifyItemChanged(positionInList, mapsOfPayloads);

                                } else // Already Voted the other option
                                    Toast.makeText(MainActivity.this, "You already submitted your vote for this item", Toast.LENGTH_SHORT).show();

                            } else {//Error
                                stopLoading();
                                Log.e("voteItemOption : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                Toast.makeText(MainActivity.this, "Error while submitting vote",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            stopLoading();
                            Log.e("voteItemOption", Objects.requireNonNull(e.getMessage()));
                            Toast.makeText(MainActivity.this, "Error while submitting vote", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        stopLoading();
                        Log.e("voteItemOption",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        Toast.makeText(MainActivity.this, "Can't reach database", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private boolean isLoading() {
        return findViewById(R.id.progress_bar).getVisibility() == VISIBLE || smartRefreshLayout.isRefreshing();
    }

    private void setLoading(boolean recyclerViewEmpty) {
        findViewById(R.id.connection_error_image).setVisibility(View.GONE);
        if (recyclerViewEmpty)
            findViewById(R.id.progress_bar).setVisibility(VISIBLE);
        else
            findViewById(R.id.loading_overlay).setVisibility(VISIBLE);

        //else smartRefreshLayout.autoRefreshAnimationOnly();
    }

    private void stopLoading() {
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
        if (smartRefreshLayout.isRefreshing()) smartRefreshLayout.finishRefresh();

    }


    /*private void requireUserCompleteRegistration(Utils.RequirementCallbacks callbacks) {
        AndroidNetworking.get(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.CHECK_USER_COMPLETED_REGISTRATION)
                .addQueryParameter("userId", String.valueOf(loggedInUser.getId()))
                .setPriority(Priority.HIGH)
                .setTag("CHECK_USER_COMPLETED_REGISTRATION")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("reqUserCompleteReg", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                boolean userCompletedRegistration = response.getBoolean("content");
                                if (userCompletedRegistration) callbacks.doWhenRequirementFulfilled();
                                else {
                                    Intent intent = new Intent(MainActivity.this,
                                            CompleteRegistrationActivity.class);
                                    intent.putExtra("loggedInUser", loggedInUser);
                                    startActivityForResult(intent, COMPLETE_REGISTRATION_REQUEST);
                                }
                            } else {//Error
                                callbacks.onRequirementFulfillingFailed(
                                        new Exception(response.getString("resultString")));
                            }

                        } catch (JSONException e) {
                            callbacks.onRequirementFulfillingFailed(e);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        //TODO Here implement can't reach database response
                        callbacks.onRequirementFulfillingFailed(
                                new Exception(anError.getErrorDetail() + ": " + anError.getMessage()));
                    }
                });

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == LOGIN_REQUEST) {
            if (resultCode == RESULT_OK)
                if (data != null) {
                    setLoading(true);

                    this.loggedInUser = (User) data.getSerializableExtra("user");
                    requireUserCompleteRegistration(new Utils.RequirementCallbacks() {
                        @Override
                        public void doWhenRequirementFulfilled() {
                            new LoadDataFromDatabase(MainActivity.this).execute();

                        }

                        @Override
                        public void onRequirementFulfillingFailed(Exception e) {
                            Log.e("reqUserCompleteReg", Objects.requireNonNull(e.getMessage()));
                            stopLoading();
                            ImageView connectionErrorImage = findViewById(R.id.connection_error_image);
                            connectionErrorImage.setVisibility(VISIBLE);

                            Snackbar.make(rootView, "An error occurred.",
                                    BaseTransientBottomBar.LENGTH_INDEFINITE)
                                    .setAction("Retry", v -> {
                                        setLoading(true);
                                        requireUserCompleteRegistration(this);
                                    })
                                    .show();
                        }
                    });

                } else {
                    Toast.makeText(this, "User login failed at last time", Toast.LENGTH_SHORT).show();
                    ensureUserLoggedIn();
                }
        } else if (requestCode == COMPLETE_REGISTRATION_REQUEST) {
            if (resultCode == RESULT_OK) {
                new LoadDataFromDatabase(MainActivity.this).execute();
            } else if (resultCode == RESULT_CANCELED)
                finish();
        } else */
        if (requestCode == NEW_BATTLE_REQUEST) {
            if (resultCode == RESULT_OK) {
                new LoadDataFromDatabase(this, false).execute();
            } else if (resultCode == NEW_BATTLE_RESULT_CONNECTION_ERROR) {
                if (data != null) {
                    Exception e = (Exception) data.getSerializableExtra("exception");
                    Snackbar.make(rootView, "Error while getting data for new battle creation.",
                            BaseTransientBottomBar.LENGTH_LONG)
                            .show();
                }
            }

        }
    }
}