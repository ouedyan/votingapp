package com.natarajankr2.voting.ui;

import androidx.annotation.NonNull;

import com.aghajari.emojiview.sticker.StickerCategory;
import com.aghajari.emojiview.sticker.StickerLoader;
import com.aghajari.emojiview.sticker.StickerProvider;

class CustomStickerProvider implements StickerProvider {
    @NonNull
    @Override
    public StickerCategory[] getCategories() {
        return new StickerCategory[0];
    }

    @NonNull
    @Override
    public StickerLoader getLoader() {
        return null;
    }

    @Override
    public boolean isRecentEnabled() {
        return true;
    }
}
