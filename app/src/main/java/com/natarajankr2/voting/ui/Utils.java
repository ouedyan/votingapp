package com.natarajankr2.voting.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

public class Utils {

    private Utils(){}

    public interface OnDoubleClickListener{
        void onDoubleClick();
    }

    public interface RequirementCallbacks{
        void doWhenRequirementFulfilled();
        void onRequirementFulfillingFailed(Exception e);
    }


    public static Bitmap getViewAsBitmap(View view){
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        //TODO What does this do ????
        canvas.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(canvas);
        return bitmap;
    }

    public static void showToastForDuration(int durationInMillis, String message, Context context) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
        new CountDownTimer(durationInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                toast.show();
            }

            @Override
            public void onFinish() {
                toast.cancel();
            }
        }.start();
    }

    /**
     * Convert the given dps value in Px
     *
     * @param res Resources from which to get the display metrics
     * @param dps Value in dps to convert in Px
     */
    public static int dpToPx(Resources res, int dps) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dps, res.getDisplayMetrics());
    }

    public static boolean isVisible(View view){
        return view.getVisibility() == View.VISIBLE ;
    }

    public static void switchVisibility(View view, boolean invisibleIsGone){
        if (isVisible(view)) {
            view.setVisibility(invisibleIsGone ? View.GONE : View.INVISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }



}
