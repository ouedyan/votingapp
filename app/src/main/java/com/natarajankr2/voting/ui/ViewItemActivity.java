package com.natarajankr2.voting.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aghajari.emojiview.AXEmojiManager;
import com.aghajari.emojiview.emoji.iosprovider.AXIOSEmojiProvider;
import com.aghajari.emojiview.listener.SimplePopupAdapter;
import com.aghajari.emojiview.search.AXEmojiSearchView;
import com.aghajari.emojiview.view.AXEmojiEditText;
import com.aghajari.emojiview.view.AXEmojiPager;
import com.aghajari.emojiview.view.AXEmojiPopupLayout;
import com.aghajari.emojiview.view.AXEmojiView;
import com.aghajari.emojiview.view.AXStickerView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.natarajankr2.voting.CommentAdapter;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.model.Comment;
import com.natarajankr2.voting.model.Item;
import com.natarajankr2.voting.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

import static com.natarajankr2.voting.ConnectToDatabase.RESULT_SUCCESS;

public class ViewItemActivity extends AppCompatActivity {
    private final ArrayList<Comment> comments = new ArrayList<>();
    private User loggedInUser;
    private Item item;
    private boolean isEmojiShowing = false;

    private AXEmojiEditText emojiEditText;
    private AXEmojiPopupLayout emojiPopupLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private View rootView;

    private View option1;
    private ImageView option1Image;
    private TextView option1Title;
    private TextView option1VotesCount;
    private ImageView option1LikeIcon;
    private View option2;
    private ImageView option2Image;
    private TextView option2Title;
    private TextView option2VotesCount;
    private ImageView option2LikeIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);

        AXEmojiManager.install(this, new AXIOSEmojiProvider(this));

        loggedInUser = (User) getIntent().getSerializableExtra("user");
        item = (Item) getIntent().getSerializableExtra("item");

        rootView = findViewById(R.id.root_view);
        recyclerView = findViewById(R.id.recyclerview);
        swipeRefreshLayout = findViewById(R.id.smart_refresh);
        emojiEditText = findViewById(R.id.emojiEdit);
        emojiPopupLayout = findViewById(R.id.emojiPopupLayout);


        option1 = findViewById(R.id.option1);
        option1Image = findViewById(R.id.image_option1);
        option1Title = findViewById(R.id.title_option1);
        option1VotesCount = findViewById(R.id.votes_count_option1);
        option1LikeIcon = findViewById(R.id.ic_like_option1);
        option2 = findViewById(R.id.option2);
        option2Image = findViewById(R.id.image_option2);
        option2Title = findViewById(R.id.title_option2);
        option2VotesCount = findViewById(R.id.votes_count_option2);
        option2LikeIcon = findViewById(R.id.ic_like_option2);

        Glide.with(this)
                .load(item.getOptionImageUrl(Item.OptionChoice.OPTION1))
                .placeholder(R.color.aboveOptionBackgroundColor)
                .error(R.drawable.ic_broken_image_24)
                .thumbnail(0.1f)
                .into(option1Image);
        Glide.with(this)
                .load(item.getOptionImageUrl(Item.OptionChoice.OPTION2))
                .placeholder(R.color.belowOptionBackgroundColor)
                .error(R.drawable.ic_broken_image_24)
                .thumbnail(0.1f)
                .into(option2Image);
        option1Title.setText(item.getOptionDescription(Item.OptionChoice.OPTION1));
        option1VotesCount.setText(String.valueOf(item.getOptionVotesCount(Item.OptionChoice.OPTION1)));
        option2Title.setText(item.getOptionDescription(Item.OptionChoice.OPTION2));
        option2VotesCount.setText(String.valueOf(item.getOptionVotesCount(Item.OptionChoice.OPTION2)));

        option1LikeIcon.setImageResource(R.drawable.ic_non_voted);
        option2LikeIcon.setImageResource(R.drawable.ic_non_voted);

        if (item.loggedUserVoted(Item.OptionChoice.OPTION1))
            option1LikeIcon.setImageResource(R.drawable.ic_voted);
        if (item.loggedUserVoted(Item.OptionChoice.OPTION2))
            option2LikeIcon.setImageResource(R.drawable.ic_voted);


        CommentAdapter commentAdapter = new CommentAdapter(comments, false);
        commentAdapter.setOnCommentAppreciationListener((comment, appreciation, positionInRecyclerView) -> {
            appreciateComment(comment.getId(), loggedInUser.getId(), appreciation, positionInRecyclerView);
        });
        commentAdapter.setOnReplyListener(replyToComment -> {
            Intent intent = new Intent(this, CommentRepliesActivity.class);
            intent.putExtra("comment", replyToComment);
            intent.putExtra("loggedInUser", loggedInUser);
            intent.putExtra("replying", true);
            startActivity(intent);
        });
        commentAdapter.setOnSeeCommentRepliesListener(comment -> {
            Intent intent = new Intent(this, CommentRepliesActivity.class);
            intent.putExtra("comment", comment);
            intent.putExtra("loggedInUser", loggedInUser);
            intent.putExtra("replying", false);
            startActivity(intent);

        });
        recyclerView.setAdapter(commentAdapter);
        new LoadDataFromDatabase(this).execute();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            new LoadDataFromDatabase(ViewItemActivity.this, comments.isEmpty()).execute();
        });

        //Emoji, stickers, GIFs configuration
        AXEmojiView emojiView = new AXEmojiView(this);
        AXStickerView stickerView = new AXStickerView(this, "stickers", new CustomStickerProvider());
        EmojiGIFPage gifPage = new EmojiGIFPage(this);
        AXEmojiPager emojiPager = new AXEmojiPager(this);
        emojiPager.addPage(emojiView, R.drawable.ic_msg_panel_smiles);
        emojiPager.addPage(stickerView, R.drawable.ic_msg_panel_stickers);
        emojiPager.addPage(gifPage, R.drawable.ic_gif_panel);
        emojiPager.setEditText(emojiEditText);
        emojiPager.setSwipeWithFingerEnabled(true);
        emojiPager.setLeftIcon(R.drawable.ic_ab_search);
        /*emojiPager.setOnFooterItemClicked((view, leftIcon) -> {
            if (leftIcon) Toast.makeText(ViewItemActivity.this,"Search Clicked", Toast.LENGTH_SHORT).show();
        });*/
        AXEmojiManager.getEmojiViewTheme().setFooterEnabled(true);

        // create emoji popup
        emojiPopupLayout.initPopupView(emojiPager);

        // Emoji SearchView
        if (AXEmojiManager.isAXEmojiView(emojiPager.getPage(0))) {
            emojiPopupLayout.setSearchView(new AXEmojiSearchView(this, emojiPager.getPage(0)));
            emojiPager.setOnFooterItemClicked((view, leftIcon) -> {
                if (leftIcon) emojiPopupLayout.showSearchView();
            });
        }

        emojiEditText.setOnClickListener(view -> {
            emojiPopupLayout.openKeyboard();
            if (comments.size() - 1 > 0)
                recyclerView.smoothScrollToPosition(comments.size() - 1);
        });

        ImageView switch_kbd_emoji = findViewById(R.id.switch_kbd_emoji);
        switch_kbd_emoji.setOnClickListener(view -> {
            if (isEmojiShowing) {
                emojiPopupLayout.openKeyboard();
            } else {
                emojiPopupLayout.show();
            }
        });

        //Send Action
        findViewById(R.id.send).setOnClickListener(view -> {
            if (Objects.requireNonNull(emojiEditText.getText()).length() > 0) {
                emojiPopupLayout.dismiss();
                addComment(loggedInUser.getId(), item.getId(), emojiEditText.getText().toString());
            }
        });

        emojiPopupLayout.setPopupListener(new SimplePopupAdapter() {
            @Override
            public void onShow() {
                updateButton(true);
            }

            @Override
            public void onDismiss() {
                updateButton(false);
            }

            @Override
            public void onKeyboardOpened(int height) {
                updateButton(false);
            }

            @Override
            public void onKeyboardClosed() {
                updateButton(emojiPopupLayout.isShowing());
            }

            private void updateButton(boolean emoji) {
                if (isEmojiShowing == emoji) return;
                isEmojiShowing = emoji;
                if (emoji) {
                    switch_kbd_emoji.setImageResource(R.drawable.ic_msg_panel_kb);
                } else {
                    switch_kbd_emoji.setImageResource(R.drawable.ic_msg_panel_smiles);
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        if (!emojiPopupLayout.onBackPressed())
            super.onBackPressed();
    }

    private static class LoadDataFromDatabase extends AsyncTask<Void, Integer, ArrayList<Comment>> {
        WeakReference<ViewItemActivity> activityWeakReference;
        boolean recyclerViewEmpty = true;
        ArrayList<Comment> comments;
        Item item;

        LoadDataFromDatabase(ViewItemActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
            comments = activity.comments;
            item = activity.item;

        }

        LoadDataFromDatabase(ViewItemActivity activity, boolean isRecyclerViewEmpty) {
            activityWeakReference = new WeakReference<>(activity);
            recyclerViewEmpty = isRecyclerViewEmpty;
            comments = activity.comments;
            item = activity.item;
        }

        @Override
        protected void onPreExecute() {
            ViewItemActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.setLoading(recyclerViewEmpty);
        }

        @Override
        protected ArrayList<Comment> doInBackground(Void... params) {
            ViewItemActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return null;
            try {
                ArrayList<Comment> allComments = ConnectToDatabase.getAllItemComments(item.getId());
                comments.clear();
                for (Comment comment : allComments) {
                    comment.setLikesCount(ConnectToDatabase.getCommentAppreciationTotalCount(comment.getId(), Comment.CommentAppreciation.LIKE));
                    comment.setDislikesCount(ConnectToDatabase.getCommentAppreciationTotalCount(comment.getId(), Comment.CommentAppreciation.DISLIKE));
                    comment.setUser(ConnectToDatabase.getUser(comment.getUserId()));
                    if (ConnectToDatabase.checkUserAppreciatedComment(
                            activity.loggedInUser.getId(),
                            comment.getId(),
                            Comment.CommentAppreciation.LIKE))
                        comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.LIKE);
                    if (ConnectToDatabase.checkUserAppreciatedComment(
                            activity.loggedInUser.getId(),
                            comment.getId(),
                            Comment.CommentAppreciation.DISLIKE))
                        comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.DISLIKE);
                    comment.setRepliesCount(ConnectToDatabase.getCommentRepliesCount(comment.getId()));
                    comments.add(comment);
                }
                return comments;

            } catch (Exception e) {
                Log.e("LoadDataFromDatabase", e.toString(), e);
                return null;
            }
        }


        @Override
        protected void onPostExecute(ArrayList<Comment> result) {
            ViewItemActivity activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) return;

            activity.stopLoading();

            if (result != null) {
                Objects.requireNonNull(activity.recyclerView.getAdapter()).notifyDataSetChanged();
                if (comments.isEmpty()) activity.findViewById(R.id.no_comment_text).setVisibility(View.VISIBLE);
            } else {
                ImageView connectionErrorImage = activity.findViewById(R.id.connection_error_image);
                if (recyclerViewEmpty) {
                    connectionErrorImage.setVisibility(View.VISIBLE);
                }
                Toast.makeText(activity, "Error while getting data from database. Swipe to retry",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void appreciateComment(int commentId, int userId, Comment.CommentAppreciation commentAppreciation,
                                   int positionInRecyclerView) {
        setLoading(false);
        int appreciation = (commentAppreciation == Comment.CommentAppreciation.DISLIKE ? -1 : 1);
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.APPRECIATE_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("commentId", String.valueOf(commentId))
                .addBodyParameter("appreciation", String.valueOf(appreciation))
                .setPriority(Priority.HIGH)
                .setTag("APPRECIATE_COMMENT")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("appreciateComment", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                stopLoading();
                                if (response.getBoolean("content")) {// Successful
                                    Comment comment = comments.get(positionInRecyclerView);
                                    if (commentAppreciation == Comment.CommentAppreciation.LIKE) {
                                        if (comment.getLoggedInUserAppreciation() != commentAppreciation) {
                                            if (comment.getLoggedInUserAppreciation() != null) // User already disliked before
                                                comment.setDislikesCount(comment.getDislikesCount() - 1);

                                            comment.setLikesCount(comment.getLikesCount() + 1);
                                            comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.LIKE);
                                        } else {
                                            comment.setLikesCount(comment.getLikesCount() - 1);
                                            comment.setLoggedInUserAppreciation(null);
                                        }

                                    } else { //Dislike
                                        if (comment.getLoggedInUserAppreciation() != commentAppreciation) {
                                            if (comment.getLoggedInUserAppreciation() != null)// User already liked before
                                                comment.setLikesCount(comment.getLikesCount() - 1);

                                            comment.setDislikesCount(comment.getDislikesCount() + 1);
                                            comment.setLoggedInUserAppreciation(Comment.CommentAppreciation.DISLIKE);

                                        } else {
                                            comment.setDislikesCount(comment.getDislikesCount() - 1);
                                            comment.setLoggedInUserAppreciation(null);
                                        }
                                    }

                                    Objects.requireNonNull(recyclerView.getAdapter()).notifyItemChanged(positionInRecyclerView);

                                }
                            } else {//Error
                                stopLoading();
                                Log.e("appreciateComment : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                Toast.makeText(ViewItemActivity.this, "An error occurred",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            stopLoading();
                            Log.e("appreciateComment", Objects.requireNonNull(e.getMessage()));
                            Toast.makeText(ViewItemActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        stopLoading();
                        Log.e("appreciateComment",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        Toast.makeText(ViewItemActivity.this, "Can't reach database", Toast.LENGTH_SHORT).show();
                    }
                });

    }


    private void addComment(int userId, int itemId, String text) {
        setLoading(comments.isEmpty());
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.ADD_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("itemId", String.valueOf(itemId))
                .addBodyParameter("text", String.valueOf(text))
                .addBodyParameter("replyToCommentId", String.valueOf(ConnectToDatabase.COMMENT_NOT_A_REPLY))
                .setPriority(Priority.HIGH)
                .setTag("ADD_COMMENT")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("addComment", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                emojiEditText.setText("");
                                new LoadDataFromDatabase(ViewItemActivity.this, comments.isEmpty()).execute();

                            } else {//Error
                                stopLoading();
                                Log.e("addComment : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                Toast.makeText(ViewItemActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            stopLoading();
                            Log.e("addComment", Objects.requireNonNull(e.getMessage()));
                            Toast.makeText(ViewItemActivity.this, "An error occurred", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        stopLoading();
                        Log.e("addComment",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        Toast.makeText(ViewItemActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void setLoading(boolean recyclerViewEmpty) {
        findViewById(R.id.no_comment_text).setVisibility(View.GONE);
        findViewById(R.id.loading_overlay).setVisibility(View.VISIBLE);//for swipe to refresh
        findViewById(R.id.connection_error_image).setVisibility(View.GONE);
        if (recyclerViewEmpty)
            findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);

    }


}