package com.natarajankr2.voting.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.natarajankr2.voting.Auth;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import static com.natarajankr2.voting.ConnectToDatabase.RESULT_SUCCESS;

public class LoginActivity extends AppCompatActivity {
    
    private final ActivityResultLauncher<Intent> mStartCompleteRegistration =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == RESULT_OK) {
                            tryLogin(null);
                        }
                    });
    
    
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private Button registerButton;
    private ProgressBar loadingProgressBar;
    private View loadingOverlay;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        loadingOverlay = findViewById(R.id.loading_overlay);
        
        emailEditText = findViewById(R.id.edit_Email);
        passwordEditText = findViewById(R.id.edit_password);
        loginButton = findViewById(R.id.login_button);
        registerButton = findViewById(R.id.register_button);
        loadingProgressBar = findViewById(R.id.loading);
        
        loadingProgressBar.setVisibility(View.GONE);
        
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }
            
            @Override
            public void afterTextChanged(Editable s) {
                //if edit text text non null enable buttons l r
                if (!(emailEditText.getText().toString().length() < 5 || passwordEditText.getText().toString().length() < 4)) {
                    loginButton.setEnabled(true);
                    registerButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                    registerButton.setEnabled(false);
                }
            }
        };
        emailEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
    }
    
    public void tryRegister(View view) {
        loadingOverlay.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.VISIBLE);
        String typedUsername = emailEditText.getText().toString();
        String typedPassword = passwordEditText.getText().toString();
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.REGISTER_USER)
                .addBodyParameter("email", typedUsername)
                .addBodyParameter("password", typedPassword)
                .setTag("REGISTER_USER")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("tryRegister", response.toString());
                        try {
                            if (response.getInt("result") == ConnectToDatabase.RESULT_EMAIL_EXISTS) {
                                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                loadingProgressBar.setVisibility(View.GONE);
                                emailEditText.requestFocus();
                                emailEditText.setError("This email already exists");
                            } else if (response.getInt("result") == ConnectToDatabase.RESULT_SUCCESS) {
                                Toast.makeText(LoginActivity.this, "Your profile has been successfully created.",
                                        Toast.LENGTH_SHORT).show();
                                tryLogin(null);
                            } else {//Error
                                Log.e("tryRegister : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                loadingProgressBar.setVisibility(View.GONE);
                                Toast.makeText(LoginActivity.this, "Error while registering",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("tryRegister", Objects.requireNonNull(e.getMessage()));
                            findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                            loadingProgressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "Error while registering",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    
                    @Override
                    public void onError(ANError anError) {
                        Log.e("tryRegister", anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                        loadingProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        
    }
    
    public void tryLogin(View view) {
        loadingOverlay.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.VISIBLE);
        String typedUsername = emailEditText.getText().toString();
        String typedPassword = passwordEditText.getText().toString();
        
        
        AndroidNetworking.post(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.LOG_USER_IN)
                .addBodyParameter("email", typedUsername)
                .addBodyParameter("password", typedPassword)
                .setTag("LOG USER IN")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("tryLogin", response.toString());
                        try {
                            if (response.getInt("result") == ConnectToDatabase.RESULT_CREDENTIALS_NOT_VALID) {
                                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                loadingProgressBar.setVisibility(View.GONE);
                                passwordEditText.requestFocus();
                                passwordEditText.setError("Email or password invalid");
                            } else if (response.getInt("result") == ConnectToDatabase.RESULT_SUCCESS) {
                                Toast.makeText(LoginActivity.this, "Successfully logged in", Toast.LENGTH_SHORT).show();
                                JSONObject userJson = response.getJSONObject("content");
                                User fetchedUser = new User();
                                fetchedUser.setId(userJson.getInt("id"));
                                fetchedUser.setEmail(userJson.getString("email"));
                                Auth.loggedInUser = fetchedUser;
    
                                requireUserCompleteRegistration(userJson.getInt("id"), new Utils.RequirementCallbacks() {
                                    @Override
                                    public void doWhenRequirementFulfilled() {
                                        try {
                                            Auth.loggedInUser.setAvatarUrl(userJson.getString("avatarUrl"));
                                            Auth.loggedInUser.setName(userJson.getString("name"));
                                            Auth.loggedInUser.setPhone(userJson.getString("phone"));
                                            //Auth.loggedInUser = fetchedUser;
                                            //setResult(RESULT_OK, new Intent().putExtra("user", fetchedUser));
                                            startActivity(new Intent(LoginActivity.this,
                                                    MainActivity.class));
                                            finish();
                                        } catch (JSONException e) {
                                            Log.e("tryLogin", Objects.requireNonNull(e.getMessage()));
                                            findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                            loadingProgressBar.setVisibility(View.GONE);
                                            Toast.makeText(LoginActivity.this, "Error while logging user in",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                        
                                    }
                                    
                                    @Override
                                    public void onRequirementFulfillingFailed(Exception e) {
                                        Log.e("tryLogin", Objects.requireNonNull(e.getMessage()));
                                        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                        loadingProgressBar.setVisibility(View.GONE);
                                        Toast.makeText(LoginActivity.this, "Error while logging user in",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {//Error
                                Log.e("tryLogin : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                                loadingProgressBar.setVisibility(View.GONE);
                                Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("tryLogin", Objects.requireNonNull(e.getMessage()));
                            findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                            loadingProgressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "Error while logging user in",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    
                    @Override
                    public void onError(ANError anError) {
                        Log.e("tryLogin", anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        findViewById(R.id.loading_overlay).setVisibility(View.GONE);
                        loadingProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        
        
    }
    
    private void requireUserCompleteRegistration(int userId, Utils.RequirementCallbacks callbacks) {
        AndroidNetworking.get(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.CHECK_USER_COMPLETED_REGISTRATION)
                .addQueryParameter("userId", String.valueOf(userId))
                .setPriority(Priority.HIGH)
                .setTag("CHECK_USER_COMPLETED_REGISTRATION")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("reqUserCompleteReg", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                boolean userCompletedRegistration = response.getBoolean("content");
                                if (userCompletedRegistration) callbacks.doWhenRequirementFulfilled();
                                else {
                                    mStartCompleteRegistration.launch(new Intent(LoginActivity.this,
                                            CompleteRegistrationActivity.class));
                                    finish();
                                }
                            } else {//Error
                                callbacks.onRequirementFulfillingFailed(
                                        new Exception(response.getString("resultString")));
                            }
                            
                        } catch (JSONException e) {
                            callbacks.onRequirementFulfillingFailed(e);
                        }
                        
                    }
                    
                    @Override
                    public void onError(ANError anError) {
                        //TODO Here implement can't reach database response
                        callbacks.onRequirementFulfillingFailed(
                                new Exception(anError.getErrorDetail() + ": " + anError.getMessage()));
                    }
                });
        
    }


    /*@Override
    public void onBackPressed() {
        //super.onBackPressed(); Do nothing
    }*/
}
