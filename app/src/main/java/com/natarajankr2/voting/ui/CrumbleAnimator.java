package com.natarajankr2.voting.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class CrumbleAnimator extends ValueAnimator {
    /**
     * SEGMENT is the base of Circle-Rifts radius,and it's also
     * the step when warp the Beeline-Rifts.
     * Set it to zero to disable the Circle-Rifts effect.
     */
    private int SEGMENT;

    static final int STAGE_BREAKING = 1;
    static final int STAGE_EARLY_END = 3;
    static final int STAGE_FALLING = 2;
    static final int STAGE_TEST = 0;
    private int stage = STAGE_BREAKING;

    private CrumblingView crumbleView;
    private View mView;
    private ViewGroup mParentView;
    private Config mConfig;
    private Bitmap mBitmap;
    private Point mTouchPoint;

    // Used in onDraw()
    private Paint onDrawPaint;
    private Path onDrawPath;
    private PathMeasure onDrawPM;

    private boolean canReverse = false;
    private boolean hasCircleRifts = true;

    private final LinePath[] lineRifts;
    private final Path[] circleRifts;
    private final int[] circleWidth;
    private final ArrayList<Path> pathArray;
    private Piece[] pieces;

    // The touch position relative to mView
    private int offsetX;
    private int offsetY;

    private OnCrumbleListener onCrumbleListener = null;
    private final Resources res;
    private boolean stageBreakingFirstStart = true;
    private boolean stageFallingFirstStart = true;

    public interface OnCrumbleListener {
        void onCancel(View view);

        void onCancelEnd(View view);

        void onFalling(View view);

        void onFallingEnd(View view);

        void onRestart(View view);

        void onStart(View view);
    }


    private CrumbleAnimator(View view, Config config) {
        mParentView = (ViewGroup) view.getParent();
        if (mParentView == null) {
            mParentView = (ViewGroup) view.getRootView();
        }

        mView = view;
        res = view.getResources();
        mBitmap = Utils.convertViewToBitmap(view);
        mTouchPoint = new Point(view.getWidth() / 2, view.getHeight() / 2);
        mConfig = config;

        crumbleView = new CrumblingView(this, view.getContext());
        mParentView.addView(crumbleView, new LayoutParams(view.getWidth(), view.getHeight()));
        crumbleView.setX(view.getX());
        crumbleView.setY(view.getY());
        crumbleView.setZ(view.getZ());
        crumbleView.setRotation(view.getRotation());
        crumbleView.setRotationX(view.getRotationX());
        crumbleView.setRotationY(view.getRotationY());

        pathArray = new ArrayList<>();
        onDrawPath = new Path();
        onDrawPM = new PathMeasure();
        lineRifts = new LinePath[mConfig.getComplexity()];
        circleRifts = new Path[mConfig.getComplexity()];
        circleWidth = new int[mConfig.getComplexity()];
        SEGMENT = mConfig.getCircleRiftsRadius();
        if (SEGMENT == 0) {
            hasCircleRifts = false;
            SEGMENT = 66;
        }

        Rect viewRect = new Rect();
        mView.getDrawingRect(viewRect);
        offsetX = mTouchPoint.x - viewRect.left;
        offsetY = mTouchPoint.y - viewRect.top;
        // Make the touchPoint be the origin of coordinates
        viewRect.offset(-mTouchPoint.x, -mTouchPoint.y);

        buildBrokenLines(viewRect);
        buildBrokenAreas(viewRect);
        buildPieces();
        buildPaintShader();
        warpStraightLines();

        setFloatValues(0, 1);
        setInterpolator(new AccelerateInterpolator(2));
        setDuration(mConfig.getBreakDuration());

        addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mParentView.removeView(crumbleView);
                if (onCrumbleListener != null) {
                    if (getStage() == STAGE_BREAKING) {
                        onCrumbleListener.onCancelEnd(mView);
                    } else if (getStage() == STAGE_FALLING) {
                        onCrumbleListener.onFallingEnd(mView);
                    }
                }

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                super.onAnimationRepeat(animation);
                setInterpolator(new LinearInterpolator());
                setStage(STAGE_FALLING);
                setFallingDuration();
                if (mConfig.isHideOriginalViewOnAnimation()) {
                    mView.setVisibility(View.INVISIBLE);
                }
                if (onCrumbleListener != null)
                    onCrumbleListener.onFalling(mView);

            }
        });
    }

    public static CrumbleAnimator get(View view, Config config) {
        return new CrumbleAnimator(view, config);
    }


    /**
     * Build beelines according to the angle
     */
    private void buildBaselines(LinePath[] baseLines, Rect rect) {
        for (int i = 0; i < mConfig.complexity; i++) {
            baseLines[i] = new LinePath();
            baseLines[i].moveTo(0, 0);
        }
        buildFirstLine(baseLines[0], rect);

        // First angle
        int angle = (int) (Math.toDegrees(Math.atan((float) (-baseLines[0].getEndY()) / baseLines[0].getEndX())));

        // The four diagonal angle base
        int[] angleBase = new int[4];
        angleBase[0] = (int) (Math.toDegrees(Math.atan((float) (-rect.top) / (rect.right))));
        angleBase[1] = (int) (Math.toDegrees(Math.atan((float) (-rect.top) / (-rect.left))));
        angleBase[2] = (int) (Math.toDegrees(Math.atan((float) (rect.bottom) / (-rect.left))));
        angleBase[3] = (int) (Math.toDegrees(Math.atan((float) (rect.bottom) / (rect.right))));

        if (baseLines[0].getEndX() < 0) // 2-quadrant,3-quadrant
            angle += 180;
        else if (baseLines[0].getEndX() > 0 && baseLines[0].getEndY() > 0) // 4-quadrant
            angle += 360;

        // Random angle range
        int range = 360 / mConfig.complexity / 3;
        int angleRandom;

        for (int i = 1; i < mConfig.complexity; i++) {
            angle = angle + 360 / mConfig.complexity;
            if (angle >= 360)
                angle -= 360;

            angleRandom = angle + Utils.nextInt(-range, range);
            if (angleRandom >= 360)
                angleRandom -= 360;
            else if (angleRandom < 0)
                angleRandom += 360;

            baseLines[i].obtainEndPoint(angleRandom, angleBase, rect);
            baseLines[i].lineToEnd();
        }

    }

    /**
     * Build broken area into path
     */
    private void buildBrokenAreas(Rect r) {
        final int SEGMENT_LESS = SEGMENT * 7 / 9;
        final int START_LENGTH = (int) (SEGMENT * 1.1);

        // The Circle-Rifts is just some isosceles triangles,
        // "linkLen" is the length of oblique side
        float linkLen = 0;
        int repeat = 0;

        PathMeasure pathMeasureNow = new PathMeasure();
        PathMeasure pathMeasurePre = new PathMeasure();

        for (int i = 0; i < mConfig.complexity; i++) {

            lineRifts[i].setStartLength(Utils.dp2px(res, START_LENGTH));

            if (repeat > 0) {
                repeat--;
            } else {
                linkLen = Utils.nextInt(Utils.dp2px(res, SEGMENT_LESS), Utils.dp2px(res, SEGMENT));
                repeat = Utils.nextInt(3);
            }

            int indexPre = (i - 1) < 0 ? mConfig.complexity - 1 : i - 1;
            pathMeasureNow.setPath(lineRifts[i], false);
            pathMeasurePre.setPath(lineRifts[indexPre], false);

            if (hasCircleRifts && pathMeasureNow.getLength() > linkLen && pathMeasurePre.getLength() > linkLen) {

                float[] pointNow = new float[2];
                float[] pointPre = new float[2];
                circleWidth[i] = Utils.nextInt(Utils.dp2px(res, 1)) + 1;
                circleRifts[i] = new Path();
                pathMeasureNow.getPosTan(linkLen, pointNow, null);
                circleRifts[i].moveTo(pointNow[0], pointNow[1]);
                pathMeasurePre.getPosTan(linkLen, pointPre, null);
                circleRifts[i].lineTo(pointPre[0], pointPre[1]);

                // The area outside Circle-Rifts
                Path pathArea = new Path();
                pathMeasurePre.getSegment(linkLen, pathMeasurePre.getLength(), pathArea, true);
                pathArea.rLineTo(0, 0); // KITKAT(API 19) and earlier need it
                drawBorder(pathArea, lineRifts[indexPre].getEndPoint(),
                        lineRifts[i].points.get(lineRifts[i].points.size() - 1), r);
                for (int j = lineRifts[i].points.size() - 2; j >= 0; j--)
                    pathArea.lineTo(lineRifts[i].points.get(j).x, lineRifts[i].points.get(j).y);
                pathArea.lineTo(pointNow[0], pointNow[1]);
                pathArea.lineTo(pointPre[0], pointPre[1]);
                pathArea.close();
                pathArray.add(pathArea);

                // The area inside Circle-Rifts, it's a isosceles triangles
                pathArea = new Path();
                pathArea.moveTo(0, 0);
                pathArea.lineTo(pointPre[0], pointPre[1]);
                pathArea.lineTo(pointNow[0], pointNow[1]);
                pathArea.close();
                pathArray.add(pathArea);

            } else {
                // Too short, there is no Circle-Rifts
                Path pathArea = new Path(lineRifts[indexPre]);
                drawBorder(pathArea, lineRifts[indexPre].getEndPoint(), lineRifts[i].points.get(lineRifts[i].points.size() - 1), r);
                for (int j = lineRifts[i].points.size() - 2; j >= 0; j--)
                    pathArea.lineTo(lineRifts[i].points.get(j).x, lineRifts[i].points.get(j).y);
                pathArea.close();
                pathArray.add(pathArea);
            }
        }
    }

    /**
     * Build warped-lines according to the baselines, like the DiscretePathEffect.
     */
    private void buildBrokenLines(Rect rect) {
        LinePath[] baseLines = new LinePath[mConfig.getComplexity()];
        buildBaselines(baseLines, rect);
        PathMeasure pathMeasureTemp = new PathMeasure();

        for (int i = 0; i < mConfig.getComplexity(); ++i) {
            lineRifts[i] = new LinePath();
            lineRifts[i].moveTo(0, 0);
            lineRifts[i].setEndPoint(baseLines[i].getEndPoint());

            pathMeasureTemp.setPath(baseLines[i], false);
            float length = pathMeasureTemp.getLength();
            int THRESHOLD = SEGMENT + SEGMENT / 2;

            if (length > Utils.dp2px(res, THRESHOLD)) {
                lineRifts[i].setStraight(false);
                // First, line to the point at SEGMENT of baseline;
                // Second, line to the random-point at (SEGMENT+SEGMENT/2) of baseline;
                // So when we set the start-draw-length to SEGMENT and the paint style is "FILL",
                // we can make the line become visible faster(exactly, the triangle)
                float[] pos = new float[2];
                pathMeasureTemp.getPosTan((float) Utils.dp2px(res, SEGMENT), pos, null);
                lineRifts[i].lineTo(pos[0], pos[1]);

                lineRifts[i].points.add(new Point((int) pos[0], (int) pos[1]));
                THRESHOLD = Utils.dp2px(res, THRESHOLD);

                int xRandom, yRandom;
                int step = Utils.dp2px(res, THRESHOLD);
                do {
                    pathMeasureTemp.getPosTan(step, pos, null);
                    // !!!
                    // Here determine the stroke width of lineRifts
                    xRandom = (int) (pos[0] + (float) Utils.nextInt(-Utils.dp2px(res, 3), Utils.dp2px(res, 2)));
                    yRandom = (int) (pos[1] + (float) Utils.nextInt(-Utils.dp2px(res, 2), Utils.dp2px(res, 3)));
                    lineRifts[i].lineTo(xRandom, yRandom);
                    lineRifts[i].points.add(new Point(xRandom, yRandom));
                    step += Utils.dp2px(res, SEGMENT);

                } while ((float) step < length);

                lineRifts[i].lineToEnd();

            } else {
                // Too short, it's still a beeline, so we must warp it later {@warpStraightLines()},
                // to make sure it is visible in "FILL" mode.
                lineRifts[i] = baseLines[i];
                lineRifts[i].setStraight(true);
            }

            lineRifts[i].points.add(lineRifts[i].getEndPoint());
        }

    }

    /**
     * Line to the the farthest boundary, in case appear a super big piece.
     */
    private void buildFirstLine(LinePath path, Rect rect) {
        int[] range = new int[]{-rect.left, -rect.top, rect.right, rect.bottom};
        int max = -1;
        int maxId = 0;
        for (int i = 0; i < 4; i++) {
            if (range[i] > max) {
                max = range[i];
                maxId = i;
            }
        }
        switch (maxId) {
            case 0:
                path.setEndPoint(rect.left, Utils.nextInt(rect.height()) + rect.top);
                break;
            case 1:
                path.setEndPoint(Utils.nextInt(rect.width()) + rect.left, rect.top);
                break;
            case 2:
                path.setEndPoint(rect.right, Utils.nextInt(rect.height()) + rect.top);
                break;
            case 3:
                path.setEndPoint(Utils.nextInt(rect.width()) + rect.left, rect.bottom);
                break;
        }
        path.lineToEnd();
    }

    private void buildPaintShader() {
        if (mConfig.paint == null) {
            onDrawPaint = new Paint();
            BitmapShader shader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            Matrix matrix = new Matrix();
            // Refraction effect
            matrix.setTranslate(-offsetX - 10, -offsetY - 7);
            shader.setLocalMatrix(matrix);
            ColorMatrix cMatrix = new ColorMatrix();
            // Increase saturation and brightness
            cMatrix.set(new float[]{
                    2.5f, 0, 0, 0, 100,
                    0, 2.5f, 0, 0, 100,
                    0, 0, 2.5f, 0, 100,
                    0, 0, 0, 1, 0});
            onDrawPaint.setColorFilter(new ColorMatrixColorFilter(cMatrix));
            onDrawPaint.setShader(shader);
            onDrawPaint.setStyle(Paint.Style.FILL);
        } else
            onDrawPaint = mConfig.paint;
    }


    /**
     * Build the final bitmap-pieces to draw in animation
     */
    private void buildPieces() {
        pieces = new Piece[pathArray.size()];

        Paint paint = new Paint();
        Matrix matrix = new Matrix();
        Canvas canvas = new Canvas();
        for (int i = 0; i < pieces.length; i++) {
            int shadow = Utils.nextInt(Utils.dp2px(res, 2), Utils.dp2px(res, 9));
            Path path = pathArray.get(i);
            RectF r = new RectF();
            path.computeBounds(r, true);

            Bitmap pBitmap = Utils.createBitmapSafely((int) r.width() + shadow * 2,
                    (int) r.height() + shadow * 2, Bitmap.Config.ARGB_4444, 1);
            if (pBitmap == null) {
                pieces[i] = new Piece(-1, -1, mView.getHeight(), null, shadow);
                continue;
            }
            pieces[i] = new Piece(
                    (int) r.left + mTouchPoint.x - shadow,
                    (int) r.top + mTouchPoint.y - shadow,
                    mView.getHeight(),
                    pBitmap,
                    shadow);
            canvas.setBitmap(pieces[i].bitmap);
            BitmapShader mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            matrix.reset();
            matrix.setTranslate(-r.left - offsetX + shadow, -r.top - offsetY + shadow);
            mBitmapShader.setLocalMatrix(matrix);

            paint.reset();
            Path offsetPath = new Path();
            offsetPath.addPath(path, -r.left + shadow, -r.top + shadow);

            // Draw shadow
            paint.setStyle(Paint.Style.FILL);
            paint.setShadowLayer(shadow, 0, 0, 0xff333333);
            canvas.drawPath(offsetPath, paint);
            paint.setShadowLayer(0, 0, 0, 0);

            // In case the view has alpha channel
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
            canvas.drawPath(offsetPath, paint);
            paint.setXfermode(null);

            // Draw bitmap
            paint.setShader(mBitmapShader);
            paint.setAlpha(0xcc);
            canvas.drawPath(offsetPath, paint);
        }
        // Sort by shadow
        Arrays.sort(pieces);

    }


    /**
     * Make sure it can be seen in "FILL" mode
     */
    private void warpStraightLines() {
        PathMeasure pathMeasureTemp = new PathMeasure();
        for (int i = 0; i < mConfig.complexity; i++) {
            if (lineRifts[i].isStraight()) {
                pathMeasureTemp.setPath(lineRifts[i], false);
                lineRifts[i].setStartLength(pathMeasureTemp.getLength() / 2);
                float[] pos = new float[2];
                pathMeasureTemp.getPosTan(pathMeasureTemp.getLength() / 2, pos, null);
                int xRandom = (int) (pos[0] + Utils.nextInt(-Utils.dp2px(res, 1), Utils.dp2px(res, 1)));
                int yRandom = (int) (pos[1] + Utils.nextInt(-Utils.dp2px(res, 1), Utils.dp2px(res, 1)));
                lineRifts[i].reset();
                lineRifts[i].moveTo(0, 0);
                lineRifts[i].lineTo(xRandom, yRandom);
                lineRifts[i].lineToEnd();
            }
        }
    }

    public boolean doReverse() {
        if (canReverse) reverse();
        return canReverse;
    }

    public boolean draw(Canvas canvas) {

        if (!isStarted()) return false;

        if (stageBreakingFirstStart) {
            setCurrentPlayTime(0);
            setRepeatCount(1);
            stageBreakingFirstStart = false;
        }

        float fraction = getAnimatedFraction();

        if (getStage() == STAGE_BREAKING) {
            canvas.save();
            canvas.translate(mTouchPoint.x, mTouchPoint.y);

            for (int i = 0; i < mConfig.getComplexity(); ++i) {
                onDrawPaint.setStyle(Style.FILL);
                onDrawPath.reset();
                onDrawPM.setPath(lineRifts[i], false);
                float pathLength = onDrawPM.getLength();
                float startLength = lineRifts[i].getStartLength();
                float drawLength = startLength + fraction * (pathLength - startLength);
                if (drawLength > pathLength) {
                    drawLength = pathLength;
                }
                onDrawPM.getSegment(0, drawLength, onDrawPath, false);
                onDrawPath.rLineTo(0, 0); // KITKAT(API 19) and earlier need it
                canvas.drawPath(onDrawPath, onDrawPaint);

                if (hasCircleRifts && circleRifts[i] != null && fraction > 0.1) {
                    onDrawPaint.setStyle(Style.STROKE);
                    float t = (fraction - 0.1f) * 2;
                    if (t > 1) t = 1;
                    onDrawPaint.setStrokeWidth(circleWidth[i] * t);
                    canvas.drawPath(circleRifts[i], onDrawPaint);
                }
            }
            canvas.restore();

        } else if (getStage() == STAGE_FALLING) {
            if (stageFallingFirstStart) {
                setRepeatCount(0);
                setCurrentPlayTime(0);
                fraction = getAnimatedFraction();
                stageFallingFirstStart = false;
            }

            int piecesNum = pieces.length;

            for (Piece piece : pieces) {
                if (piece.bitmap != null && piece.advance(fraction))
                    canvas.drawBitmap(piece.bitmap, piece.matrix, null);
                else
                    piecesNum--;
            }
            if (piecesNum == 0) {
                setStage(STAGE_EARLY_END);
                if (onCrumbleListener != null) onCrumbleListener.onFallingEnd(mView);
            }

        } else if (getStage() == STAGE_TEST) {
            float t = (float)1 / pieces.length;
            int drawNum = (int)(fraction / t);
            for(int i = 0; i <= drawNum; i++) {
                if(pieces[i].bitmap != null)
                    canvas.drawBitmap(pieces[i].bitmap, pieces[i].matrix, null);
            }
        }

        crumbleView.invalidate();
        return true;
    }

    public void drawBorder(Path path, Point pointStart, Point pointEnd, Rect rect) {

        if (pointStart.x == rect.right) {
            if (pointEnd.x == rect.right) {
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.top) {
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.left) {
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.bottom) {
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            }

        } else if (pointStart.y == rect.top) {
            if (pointEnd.y == rect.top) {
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.left) {
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.bottom) {
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.right) {
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            }
        } else if (pointStart.x == rect.left) {
            if (pointEnd.x == rect.left) {
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.bottom) {
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.right) {
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.top) {
                path.lineTo((float) rect.left, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            }
        } else if (pointStart.y == rect.bottom) {
            if (pointEnd.y == rect.bottom) {
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.right) {
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.y == rect.top) {
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            } else if (pointEnd.x == rect.left) {
                path.lineTo((float) rect.right, (float) rect.bottom);
                path.lineTo((float) rect.right, (float) rect.top);
                path.lineTo((float) rect.left, (float) rect.top);
                path.lineTo((float) pointEnd.x, (float) pointEnd.y);
            }
        }

    }

    public int getStage() {
        return stage;
    }

    public void reset() {
        removeAllListeners();
        cancel();
        crumbleView.invalidate();
    }

    private void setFallingDuration() {
        this.setDuration(mConfig.getFallDuration());
    }

    public void setOnCrumbleListener(OnCrumbleListener onCrumbleListener) {
        this.onCrumbleListener = onCrumbleListener;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    @Override
    public void start() {
        super.start();
        this.stageFallingFirstStart = true;
        this.stageBreakingFirstStart = true;
        if (onCrumbleListener != null) {
            onCrumbleListener.onStart(mView);
            if (doReverse()) {
                onCrumbleListener.onRestart(mView);
            }
        }
        canReverse = true;
        crumbleView.invalidate();
    }

    public static class Config {
        private int breakDuration = 700;
        private int circleRiftsRadius = 66;
        private int complexity = 12;
        private int fallDuration = 2000;
        private boolean hideOriginalViewOnAnimation = true;
        private Paint paint = null;

        public int getBreakDuration() {
            return this.breakDuration;
        }

        public int getCircleRiftsRadius() {
            return this.circleRiftsRadius;
        }

        public int getComplexity() {
            return this.complexity;
        }

        public int getFallDuration() {
            return this.fallDuration;
        }

        public Paint getPaint() {
            return this.paint;
        }

        public boolean isHideOriginalViewOnAnimation() {
            return this.hideOriginalViewOnAnimation;
        }

        public Config setBreakDuration(int duration) {
            this.breakDuration = Math.max(duration, 200);
            return this;
        }

        public Config setCircleRiftsRadius(int circleRiftsRadius) {
            this.circleRiftsRadius = circleRiftsRadius < 20 && circleRiftsRadius != 0 ? 20 : circleRiftsRadius;
            return this;
        }

        public Config setComplexity(int complexity) {
            this.complexity = Math.min(20, Math.max(complexity, 6));
            return this;
        }

        public Config setFallDuration(int duration) {
            this.fallDuration = Math.max(duration, 200);
            return this;
        }

        public Config setHideOriginalViewOnAnimation(boolean hide) {
            this.hideOriginalViewOnAnimation = hide;
            return this;
        }

        public Config setPaint(Paint paint) {
            this.paint = paint;
            return this;
        }
    }

    class CrumblingView extends View {
        final CrumbleAnimator crumbleAnimator;

        public CrumblingView(CrumbleAnimator crumbleAnimator, Context context) {
            this(crumbleAnimator, context, null);
        }

        public CrumblingView(CrumbleAnimator crumbleAnimator, Context context, AttributeSet attrs) {
            this(crumbleAnimator, context, attrs, 0);
        }

        public CrumblingView(CrumbleAnimator crumbleAnimator, Context context, AttributeSet attrs, int defStyleAttr) {
            this(crumbleAnimator, context, attrs, defStyleAttr, 0);
        }

        public CrumblingView(CrumbleAnimator crumbleAnimator, Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
            this.crumbleAnimator = crumbleAnimator;
            this.setLayerType(LAYER_TYPE_HARDWARE, null);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            this.crumbleAnimator.draw(canvas);
        }
    }

    private static class Utils {
        private static final Random random = new Random();

        private Utils() {
        }

        static Bitmap convertViewToBitmap(View view) {
            view.clearFocus();
            Bitmap bitmap = createBitmapSafely(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888, 2);
            if (bitmap != null) {
                Canvas canvas = new Canvas();
                canvas.setBitmap(bitmap);
                canvas.translate(-view.getScrollX(), -view.getScrollY());
                view.draw(canvas);
                canvas.setBitmap(null);
            }

            return bitmap;
        }

        static Bitmap createBitmapSafely(int width, int height, Bitmap.Config bitmapConfig, int retryCount) {
            while(retryCount > 0) {
                try {
                    return Bitmap.createBitmap(width, height, bitmapConfig);
                    
                } catch (OutOfMemoryError outOfMemoryError) {
                    outOfMemoryError.printStackTrace();
                    System.gc();
                    --retryCount;
                }
            }

            return null;
        }

        static int dp2px(Resources res, int dp) {
            return (int) TypedValue.applyDimension(1, (float)dp, res.getDisplayMetrics());
        }

        static boolean nextBoolean() {
            return random.nextBoolean();
        }

        static float nextFloat(float a) {
            return random.nextFloat() * a;
        }

        static float nextFloat(float a, float b) {
            return Math.min(a, b) + random.nextFloat() * Math.abs(a - b);
        }

        static int nextInt(int a) {
            return random.nextInt(a);
        }

        static int nextInt(int a, int b) {
            return Math.min(a, b) + random.nextInt(Math.abs(a - b));
        }
    }

    class Piece implements Comparable<Piece> {
        Bitmap bitmap;
        Matrix matrix;
        private int x;
        private int y;
        private int rotateX;
        private int rotateY;
        private float angle;
        private float speed;
        private int shadow;
        private int limitY;
        private int parentHeight;

        public Piece(int x, int y, int parentHeight, Bitmap bitmap, int shadow) {
            this.bitmap = bitmap;
            this.x = x;
            this.y = y;
            this.shadow = shadow;

            if (bitmap != null) {
                matrix = new Matrix();
                matrix.postTranslate(x, y);

                speed = Utils.nextFloat(1, 4);
                rotateX = Utils.nextInt(bitmap.getWidth());
                rotateY = Utils.nextInt(bitmap.getHeight());
                angle = Utils.nextFloat(0.6f) * (Utils.nextBoolean() ? 1 : -1);

                this.parentHeight = parentHeight;
                limitY = Math.max(bitmap.getWidth(), bitmap.getHeight());
                limitY += parentHeight;
            }

        }

        public boolean advance(float fraction) {
            float s = (float)Math.pow(fraction * 1.1226f, 2) * 8 * speed;
            float zy = y + s * parentHeight / 10;
            float r = fraction * fraction;

            matrix.reset();
            matrix.setRotate(angle * r * 360, rotateX, rotateY);
            matrix.postTranslate(x, zy);

            return zy <= limitY;
        }

        @Override
        public int compareTo(Piece anotherPiece) {
            return shadow - anotherPiece.shadow;
        }
    }

    class LinePath extends Path {
        private Point endPoint;
        public ArrayList<Point> points;
        private float startLength;
        private boolean straight;

        public LinePath() {
            super();
            points = new ArrayList<>();
            startLength = -1;
            endPoint = new Point();
        }

        public LinePath(LinePath path) {
            super(path);
            points = (ArrayList<Point>) path.points.clone();
            startLength = path.getStartLength();
            endPoint = new Point(path.getEndPoint());
        }

        public Point getEndPoint() {
            return endPoint;
        }

        public int getEndX() {
            return endPoint.x;
        }

        public int getEndY() {
            return endPoint.y;
        }

        public float getStartLength() {
            return startLength;
        }

        public boolean isStraight() {
            return straight;
        }

        public void lineToEnd() {
            lineTo(endPoint.x, endPoint.y);
        }

        public void setEndPoint(int endX, int endY) {
            endPoint.set(endX, endY);
        }

        public void setEndPoint(Point endPoint) {
            this.endPoint = endPoint;
        }

        public void setStartLength(float startLength) {
            this.startLength = startLength;
        }

        public void setStraight(boolean isStraight) {
            straight = isStraight;
        }

        public void obtainEndPoint(int angleRandom,int[] angleBase,Rect r){

            float gradient = -(float) Math.tan(Math.toRadians(angleRandom));

            int endX = 0, endY = 0;

            if (angleRandom >= 0 && angleRandom < 90) {
                if (angleRandom < angleBase[0]) {
                    endX = r.right;
                    endY = (int)(endX * gradient);
                } else if (angleRandom > angleBase[0]) {
                    endY = r.top;
                    endX = (int) (endY / gradient);
                } else if (angleRandom == angleBase[0]) {
                    endY = r.top;
                    endX = r.right;
                }
            } else if (angleRandom > 90 && angleRandom <= 180) {
                if (180 - angleRandom < angleBase[1]) {
                    endX = r.left;
                    endY = (int) (endX * gradient);
                } else if (180 - angleRandom > angleBase[1]) {
                    endY = r.top;
                    endX = (int) (endY / gradient);
                } else if (180 - angleRandom == angleBase[1]) {
                    endY = r.top;
                    endX = r.left;
                }
            } else if (angleRandom > 180 && angleRandom < 270) {
                if (angleRandom - 180 < angleBase[2]) {
                    endX = r.left;
                    endY = (int) (endX * gradient);
                } else if (angleRandom - 180 > angleBase[2]) {
                    endY = r.bottom;
                    endX = (int) (endY / gradient);
                } else if (angleRandom - 180 == angleBase[2]) {
                    endY = r.bottom;
                    endX = r.left;
                }
            } else if (angleRandom > 270 && angleRandom < 360) {
                if (360 - angleRandom < angleBase[3]) {
                    endX = r.right;
                    endY = (int) (endX * gradient);
                } else if (360 - angleRandom > angleBase[3]) {
                    endY = r.bottom;
                    endX = (int) (endY / gradient);
                } else if (360 - angleRandom == angleBase[3]) {
                    endY = r.bottom;
                    endX = r.right;
                }
            }
            else if(angleRandom == 90) {
                endX = 0;
                endY = r.top;
            }
            else if(angleRandom == 270) {
                endX = 0;
                endY = r.bottom;
            }
            endPoint.set(endX,endY);
        }
    }

}
