package com.natarajankr2.voting.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.listener.DismissListener;
import com.google.android.material.textfield.TextInputLayout;
import com.natarajankr2.voting.ConnectToDatabase;
import com.natarajankr2.voting.R;
import com.natarajankr2.voting.WikiSuggestAdapter;
import com.natarajankr2.voting.model.Category;
import com.natarajankr2.voting.model.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import static com.natarajankr2.voting.ConnectToDatabase.RESULT_SUCCESS;
import static com.natarajankr2.voting.model.Item.OptionChoice.OPTION1;
import static com.natarajankr2.voting.model.Item.OptionChoice.OPTION2;

public class NewBattleActivity extends AppCompatActivity {
    private static final int CONNECTION_ERROR = 4;
    /**
     * ImageChooser ImagePicker library default request code
     */
    private static final int IMAGE_PICKER_DEFAULT_REQUEST_CODE = 2404;

    private TextInputLayout categoryDropdown;
    private AutoCompleteTextView categoryEdit;
    private TextInputLayout option1TitleInput;
    private AutoCompleteTextView option1TitleEdit;
    private TextInputLayout option2TitleInput;
    private AutoCompleteTextView option2TitleEdit;

    private ImageView option1Picture;
    private ImageView option1NoImageAdd;

    private ImageView option2Picture;
    private ImageView option2NoImageAdd;

    private Item.OptionChoice lastPictureRequestOption = null;
    private boolean doNotCallDialogDismissListener = false;

    private ArrayList<Category> allCategories = new ArrayList<>();
    private ArrayList<String> allCategoriesNames = new ArrayList<>();
    private Category selectedCategory = null;
    private String option1PictureTempPath = null;
    private String option2PictureTempPath = null;
    private String categoryName = null ;
    private String option1Description = null;
    private String option2Description = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_battle);

        setPageLoading(true);

        categoryDropdown = findViewById(R.id.category_dropdown);
        option1TitleInput = findViewById(R.id.field_option1_desc);
        option2TitleInput = findViewById(R.id.field_option2_desc);

        categoryEdit =
                ((AutoCompleteTextView) Objects.requireNonNull(categoryDropdown.getEditText()));
        option1TitleEdit =
                ((AutoCompleteTextView) Objects.requireNonNull(option1TitleInput.getEditText()));
        option2TitleEdit =
                ((AutoCompleteTextView) Objects.requireNonNull(option2TitleInput.getEditText()));
        //Remove other suggestions to AutoCompleteTextView : doesn't work if set in xml
        categoryEdit.setInputType(EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        //Adding clearing focus of the editor to the default #onEditorAction() behavior.
        TextView.OnEditorActionListener customOnEditorActionListener = (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                v.clearFocus();
                hideKeyboard();
            }
            return false;
        };
        categoryEdit.setOnEditorActionListener(customOnEditorActionListener);
        option1TitleEdit.setOnEditorActionListener(customOnEditorActionListener);
        option2TitleEdit.setOnEditorActionListener(customOnEditorActionListener);

        //Clearing focus after a suggestion has been selected
        categoryEdit.setOnItemClickListener((parent, view, position, id) -> {
            categoryEdit.onEditorAction(EditorInfo.IME_ACTION_DONE);
        });
        option1TitleEdit.setOnItemClickListener((parent, view, position, id) -> {
            option1TitleEdit.onEditorAction(EditorInfo.IME_ACTION_DONE);
        });
        option2TitleEdit.setOnItemClickListener((parent, view, position, id) -> {
            option2TitleEdit.onEditorAction(EditorInfo.IME_ACTION_DONE);
        });


        //Wrapping lines manually instead of automatically(putting textMultiline flag on inputType) on simple
        // editText, because textMultiline displays Enter at imeAction DONE place .
        // Setting it from xml doesn't work
        option1TitleEdit.setHorizontallyScrolling(false);
        option1TitleEdit.setMaxLines(5);
        option2TitleEdit.setHorizontallyScrolling(false);
        option2TitleEdit.setMaxLines(5);


        AndroidNetworking.get(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.GET_ALL_CATEGORIES)
                .setPriority(Priority.HIGH)
                .setTag("GET_ALL_CATEGORIES")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("getAllCategories", response.toString());
                        try {
                            if (response.getInt("result") == RESULT_SUCCESS) {
                                JSONArray categories = response.getJSONArray("content");
                                for (int i = 0; i < categories.length(); i++) {
                                    JSONObject jsonCategory = categories.getJSONObject(i);
                                    Category currentCategory = new Category();
                                    currentCategory.setId(jsonCategory.getInt("id"));
                                    currentCategory.setName(jsonCategory.getString("name"));
                                    currentCategory.setImageUrl(jsonCategory.getString("imageUrl"));
                                    allCategories.add(currentCategory);
                                    allCategoriesNames.add(jsonCategory.getString("name"));
                                }
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(NewBattleActivity.this,
                                        R.layout.dropdown_items_layout, allCategoriesNames);

                                categoryEdit.setAdapter(arrayAdapter);
                                setPageLoading(false);

                            } else {//Error
                                Log.e("getAllCategories : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                error();
                            }

                        } catch (JSONException e) {
                            Log.e("getAllCategories", Objects.requireNonNull(e.getMessage()));
                            error();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("getAllCategories",
                                anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        error();
                    }
                });

        option1TitleEdit.setAdapter(new WikiSuggestAdapter(this));
        option2TitleEdit.setAdapter(new WikiSuggestAdapter(this));

        //Images pick configuration
        option1Picture = findViewById(R.id.option1_picture);
        option1NoImageAdd = findViewById(R.id.option1_no_image_add);
        View option1PictureModificationLayer = findViewById(R.id.option1_picture_modification_layer);
        ImageView option1PictureChange = findViewById(R.id.option1_picture_change);
        ImageView option1PictureRemove = findViewById(R.id.option1_picture_remove);

        option2Picture = findViewById(R.id.option2_picture);
        option2NoImageAdd = findViewById(R.id.option2_no_image_add);
        View option2PictureModificationLayer = findViewById(R.id.option2_picture_modification_layer);
        ImageView option2PictureChange = findViewById(R.id.option2_picture_change);
        ImageView option2PictureRemove = findViewById(R.id.option2_picture_remove);

        option1NoImageAdd.setVisibility(View.VISIBLE);
        option2NoImageAdd.setVisibility(View.VISIBLE);
        option1PictureModificationLayer.setVisibility(View.INVISIBLE);
        option2PictureModificationLayer.setVisibility(View.INVISIBLE);
        setPictureLoading(false, OPTION1);
        setPictureLoading(false, OPTION2);

        option1NoImageAdd.setOnClickListener(v -> {
            addPicture(OPTION1);
        });
        option2NoImageAdd.setOnClickListener(v -> {
            addPicture(OPTION2);
        });

        option1Picture.setOnClickListener(v -> option1PictureModificationLayer.setVisibility(View.VISIBLE));
        option2Picture.setOnClickListener(v -> option2PictureModificationLayer.setVisibility(View.VISIBLE));
        option1PictureModificationLayer.setOnClickListener(v -> v.setVisibility(View.INVISIBLE));
        option2PictureModificationLayer.setOnClickListener(v -> v.setVisibility(View.INVISIBLE));

        option1PictureChange.setOnClickListener(v -> {
            option1PictureModificationLayer.setVisibility(View.INVISIBLE);
            addPicture(OPTION1);
        });
        option2PictureChange.setOnClickListener(v -> {
            option2PictureModificationLayer.setVisibility(View.INVISIBLE);
            addPicture(OPTION2);
        });

        option1PictureRemove.setOnClickListener(v -> {
            option1PictureModificationLayer.setVisibility(View.INVISIBLE);
            option1Picture.setImageResource(0);
            option1PictureTempPath = null;
            option1NoImageAdd.setVisibility(View.VISIBLE);
        });
        option2PictureRemove.setOnClickListener(v -> {
            option2PictureModificationLayer.setVisibility(View.INVISIBLE);
            option2Picture.setImageResource(0);
            option2PictureTempPath = null;
            option2NoImageAdd.setVisibility(View.VISIBLE);
        });

    }

    private void error() {
        setResult(CONNECTION_ERROR);
        finish();
    }

    private void addPicture(Item.OptionChoice option) {
        View imageProviderChooserDialog = LayoutInflater.from(this).inflate(R.layout.dialog_choose_app, null);
        AlertDialog chooserDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.title_choose_image_provider)
                .setView(imageProviderChooserDialog)
                .setOnCancelListener((dialog) -> setPictureLoading(false, option))
                .setNegativeButton(R.string.action_cancel, (dialog, viewId) -> setPictureLoading(false, option))
                .setOnDismissListener((dialog) -> {
                    if (!doNotCallDialogDismissListener) {
                        setPictureLoading(false, option);
                        doNotCallDialogDismissListener = false;
                    }
                })
                .show();
        imageProviderChooserDialog.findViewById(R.id.lytCameraPick).setOnClickListener((v) -> {
            lastPictureRequestOption = option;
            setPictureLoading(true, option);
            doNotCallDialogDismissListener = true;
            chooserDialog.dismiss();
            ImagePicker.Companion.with(this).cameraOnly().compress(1024).start();

        });
        imageProviderChooserDialog.findViewById(R.id.lytGalleryPick).setOnClickListener((v) -> {
            lastPictureRequestOption = option;
            setPictureLoading(true, option);
            doNotCallDialogDismissListener = true;
            chooserDialog.dismiss();
            ImagePicker.Companion.with(this).galleryOnly().compress(1024).start();

        });
    }

    private void setPageLoading(boolean loading) {
        findViewById(R.id.page_loading_overlay).setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void setPictureLoading(boolean loading, Item.OptionChoice option) {
        if (option == OPTION1) {
            findViewById(R.id.option1_picture_loading_layer).setVisibility(loading ? View.VISIBLE : View.GONE);
        } else if (option == OPTION2) {
            findViewById(R.id.option2_picture_loading_layer).setVisibility(loading ? View.VISIBLE : View.GONE);
        }
    }

    public void newBattle(View view) {
        if (!validateFields()) return;

        setPageLoading(true);
        int categoryId = allCategories.get(allCategoriesNames.indexOf(categoryName)).getId() ;
        AndroidNetworking.upload(ConnectToDatabase.ApiEndPoint.BASE_URL + ConnectToDatabase.ApiEndPoint.NEW_BATTLE)
                .addMultipartFile("option1Image", new File(option1PictureTempPath))
                .addMultipartFile("option2Image", new File(option2PictureTempPath))
                .addMultipartParameter("categoryId", String.valueOf(categoryId))
                .addMultipartParameter("option1Description", option1Description)
                .addMultipartParameter("option2Description", option2Description)
                .setTag("NEW_BATTLE")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("newBattle", response.toString());
                        try {
                            if (response.getInt("result") == ConnectToDatabase.RESULT_SUCCESS) {
                                setResult(RESULT_OK);
                                finish();
                            } else {//Error
                                Log.e("newBattle : ", response.getString("resultString"));
                                new Exception(response.getString("resultString")).printStackTrace();
                                setPageLoading(false);
                                Toast.makeText(NewBattleActivity.this, "Error while sending data",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("newBattle", Objects.requireNonNull(e.getMessage()));
                            setPageLoading(false);
                            Toast.makeText(NewBattleActivity.this, "Error while sending data",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("newBattle", anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
                        setPageLoading(false);
                        Toast.makeText(NewBattleActivity.this, "Can't reach database",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private boolean validateFields() {
        boolean result = true;

        categoryDropdown.setError(null);
        option1TitleInput.setError(null);
        option2TitleInput.setError(null);

        categoryName = categoryEdit.getText().toString().trim();
        option1Description = option1TitleEdit.getText().toString().trim();
        option2Description = option2TitleEdit.getText().toString().trim();

        boolean errorAbove = false;

        if (TextUtils.isEmpty(categoryName)) {
            categoryDropdown.setError("Please enter item's category");
            result = false;
            errorAbove = true;
            categoryDropdown.requestFocus();
        } else if (!allCategoriesNames.contains(categoryName)){
            categoryDropdown.setError("Please enter a valid category");
            result = false;
            errorAbove = true;
            categoryDropdown.requestFocus();
        }

        if (TextUtils.isEmpty(option1Description)) {
            option1TitleInput.setError("Please enter option 1's title");
            result = false;
            if (!errorAbove){
                errorAbove = true;
                option1TitleInput.requestFocus();
            }
        }
        if (option1PictureTempPath == null) {
            Toast.makeText(this, "Please choose an image for option 1", Toast.LENGTH_SHORT).show();
            result = false;
            if (!errorAbove){
                errorAbove = true;
                option1Picture.requestFocus();
            }
        }

        if (TextUtils.isEmpty(option2Description)) {
            option2TitleInput.setError("Please enter option 2's title");
            result = false;
            if (!errorAbove){
                errorAbove = true;
                option2TitleInput.requestFocus();
            }
        }
        if (option2PictureTempPath == null) {
            Toast.makeText(this, "Please choose an image for option 2", Toast.LENGTH_SHORT).show();
            result = false;
            if (!errorAbove){
                option2Picture.requestFocus();
            }
        }

        return result;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_PICKER_DEFAULT_REQUEST_CODE) {
            handleImagePickerResult(data, resultCode);
        }
    }

    private void handleImagePickerResult(Intent data, int resultCode) {
        if (resultCode == RESULT_OK && data != null) {
            String filePath = ImagePicker.Companion.getFilePath(data);
            //Toast.makeText(this, "Returned: File Path: " + filePath, Toast.LENGTH_SHORT).show();
            Log.i("handleImagePickerResult", "File Path: " + filePath);
            if (filePath != null) {
                if (lastPictureRequestOption == OPTION1) {
                    setPictureLoading(false, OPTION1);
                    option1NoImageAdd.setVisibility(View.GONE);
                    Glide.with(this).load(new File(filePath))
                            .into(option1Picture);
                    option1PictureTempPath = filePath;
                } else if (lastPictureRequestOption == OPTION2) {
                    setPictureLoading(false, OPTION2);
                    option2NoImageAdd.setVisibility(View.GONE);
                    Glide.with(this).load(new File(filePath))
                            .into(option2Picture);
                    option2PictureTempPath = filePath;

                }

            }
        } else {//CANCEL or another problem

            if (lastPictureRequestOption == OPTION1) {
                setPictureLoading(false, OPTION1);

            } else if (lastPictureRequestOption == OPTION2) {
                setPictureLoading(false, OPTION2);

            }

        }

        lastPictureRequestOption = null;

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE) ;
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

}