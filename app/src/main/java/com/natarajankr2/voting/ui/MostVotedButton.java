package com.natarajankr2.voting.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GestureDetectorCompat;

import static com.natarajankr2.voting.ui.Utils.getViewAsBitmap;

public class MostVotedButton extends ConstraintLayout {
    private MotionEvent lastActionDownEvent = null;
    private Utils.OnDoubleClickListener onDoubleClickListener;
    private final GestureDetectorCompat gestureDetector;
    private boolean isSingleTapConfirmed = true;

    public void setOnDoubleClickListener(Utils.OnDoubleClickListener onDoubleClickListener) {
        this.onDoubleClickListener = onDoubleClickListener;
    }


    public MostVotedButton(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);

        //Useless to return value...On GestureListener return values are considered
        GestureDetector.OnDoubleTapListener onDoubleTapListener = new GestureDetector.OnDoubleTapListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                performClick();
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                MostVotedButton.this.onDoubleClickListener.onDoubleClick();
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return false;
            }
        };

        GestureDetector.OnGestureListener onGestureListener = new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        };

        gestureDetector = new GestureDetectorCompat(context, onGestureListener);
        gestureDetector.setOnDoubleTapListener(onDoubleTapListener);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            lastActionDownEvent = MotionEvent.obtain(event);

        //TODO Library custom shaped views Remaining : Fix shadows and outline
        boolean pixelIsTransparent = getViewAsBitmap(this).getPixel((int) lastActionDownEvent.getX(),
                (int) lastActionDownEvent.getY()) == Color.TRANSPARENT;

        if (pixelIsTransparent)
            return false;
        gestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        Path path = getPath();
        /*Paint paint = new Paint();
        paint.setColor(Color.parseColor("#000000"));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        canvas.drawPath(path,paint);*/
        canvas.clipPath(path);
        super.draw(canvas);
    }

    private Path getPath() {
        Path path = new Path();
        path.setFillType(Path.FillType.WINDING);
        path.moveTo(0, (float) (0.3 * getHeight()));
        path.lineTo(0, getHeight());
        path.lineTo(getWidth(), (float) (0.15 * getHeight()));
        path.close();
        return path;
    }

}
