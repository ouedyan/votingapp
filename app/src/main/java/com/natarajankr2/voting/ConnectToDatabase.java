package com.natarajankr2.voting;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.natarajankr2.voting.model.Category;
import com.natarajankr2.voting.model.Comment;
import com.natarajankr2.voting.model.Item;
import com.natarajankr2.voting.model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

public class ConnectToDatabase {
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_ERROR = 0;
    public static final int RESULT_EMAIL_EXISTS = -1;
    public static final int RESULT_CREDENTIALS_NOT_VALID = -2;

    public static final int COMMENT_NOT_A_REPLY = -1;
    public static final int AVATAR_NULL = -1;

    public static class ApiEndPoint {



        private ApiEndPoint() {
        }

        public static final String BASE_URL = "http://43.254.41.145/voting-app/api";
        //public static final String BASE_URL = "http://192.168.43.210/voting-app/api";
        public static final String LOG_USER_IN = "/logUserIn.php";
        public static final String REGISTER_USER = "/registerUser.php";
        public static final String COMPLETE_REGISTRATION = "/completeRegistration.php";
        public static final String CHECK_USER_COMPLETED_REGISTRATION = "/checkUserCompletedRegistration.php";
        public static final String GET_ALL_CATEGORIES = "/getAllCategories.php";
        public static final String GET_ALL_ITEMS = "/getAllItems.php";
        public static final String GET_USER = "/getUser.php";
        public static final String VOTE_ITEM_OPTION = "/voteItemOption.php";
        public static final String CHECK_USER_VOTED_ITEM_OPTION = "/checkUserVotedItemOption.php";
        public static final String CHECK_USER_APPRECIATED_COMMENT = "/checkUserAppreciatedComment.php";
        public static final String GET_ITEM_VOTES_COUNT = "/getItemVotesCount.php";
        public static final String GET_ITEM_VOTE_PERCENTAGES = "/getItemVotePercentages.php";
        public static final String GET_ITEM_OPTIONS_VOTES_COUNT = "/getItemOptionsVotesCount.php";
        public static final String ADD_COMMENT = "/addComment.php";
        public static final String APPRECIATE_COMMENT = "/appreciateComment.php";
        public static final String GET_COMMENT_APPRECIATION_TOTAL_COUNT = "/getCommentAppreciationTotalCount.php";
        public static final String GET_COMMENT_REPLIES_COUNT = "/getCommentRepliesCount.php";
        public static final String GET_ALL_COMMENT_REPLIES = "/getAllCommentReplies.php";
        public static final String GET_ALL_ITEM_COMMENTS = "/getAllItemComments.php";
        public static final String NEW_BATTLE = "/newBattle.php";
    }


    enum QueryType {
        SELECT,
        UPDATE
    }

    enum RequestError {
        EMAIL_EXISTS,
        CREDENTIALS_NOT_VALID,
        ITEM_ALREADY_VOTED
    }


    /**
     * @param userId           The user's ID
     * @param itemId           The item's ID
     * @param text             The comment text
     * @param replyToCommentId The ID of the comment to which this comment replies or -1 if not replying to a
     *                         comment.
     * @return true if comment was successfully added.
     * @throws Exception if and error occurred.
     */
    public static boolean addComment(int userId, int itemId, String text, int replyToCommentId) throws Exception {
        boolean result;
        ANRequest request = AndroidNetworking.post(ApiEndPoint.BASE_URL + ApiEndPoint.ADD_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("itemId", String.valueOf(itemId))
                .addBodyParameter("text", String.valueOf(text))
                .addBodyParameter("replyToCommentId", String.valueOf(replyToCommentId))
                .setPriority(Priority.HIGH)
                .setTag("ADD_COMMENT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
                throw new Exception("addComment : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("addComment : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Check if a specific user completed registration.
     *
     * @param userId The user's ID
     * @return true if the user completed registration, else false.
     * @throws Exception if and error occurred.
     */
    public static boolean checkUserCompletedRegistration(int userId) throws Exception {
        boolean result;
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.CHECK_USER_COMPLETED_REGISTRATION)
                .addQueryParameter("userId", String.valueOf(userId))
                .setPriority(Priority.HIGH)
                .setTag("CHECK_USER_COMPLETED_REGISTRATION")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
            {
                throw new Exception("checkUserCompletedRegistration : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
            }

        } else {
            ANError anError = response.getError();
            throw new Exception("checkUserCompletedRegistration : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;
    }

    /**
     * Get a specific user's data.
     *
     * @param userId The user's ID
     * @return a User object
     * @throws Exception if and error occurred.
     */
    public static User getUser(int userId) throws Exception {
        User retrievedUser = new User();
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_USER)
                .addQueryParameter("userId", String.valueOf(userId))
                .setPriority(Priority.HIGH)
                .setTag("GET_USER")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONObject jsonUser = jsonObject.getJSONObject("content");
                /*retrievedUser.setName(jsonUser.isNull("name") ?
                        null : jsonUser.getString("name"));*/
                retrievedUser.setId(jsonUser.getInt("id"))
                        .setEmail(jsonUser.getString("email"))
                        .setName(jsonUser.getString("name"))
                        .setAvatarUrl(jsonUser.getString("avatarUrl"))
                        .setPhone(jsonUser.getString("phone"));
            } else { //Error
                throw new Exception("getUser : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
            }

        } else {
            ANError anError = response.getError();
            throw new Exception("getUser : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return retrievedUser;
    }


    /**
     * Get all categories
     *
     * @return An ArrayList of Category objects
     * @throws Exception if an error occurred.
     */
    public static ArrayList<Category> getAllCategories() throws Exception {
        ArrayList<Category> result = new ArrayList<>();
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ALL_CATEGORIES)
                .setPriority(Priority.HIGH)
                .setTag("GET_ALL_CATEGORIES")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONArray categories = jsonObject.getJSONArray("content");
                for (int i = 0; i < categories.length(); i++) {
                    JSONObject jsonCategory = categories.getJSONObject(i);
                    Category currentCategory = new Category();
                    currentCategory.setId(jsonCategory.getInt("id"));
                    currentCategory.setName(jsonCategory.getString("name"));
                    currentCategory.setImageUrl(jsonCategory.getString("imageUrl"));
                    result.add(currentCategory);
                }
            } else //Error
                throw new Exception("getAllCategories : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("getAllCategories : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;
    }

    /**
     * Get all items in the most voted descendant order
     *
     * @return An ArrayList of Item objects
     * @throws Exception if and error occurred.
     */
    public static ArrayList<Item> getAllItems() throws Exception {
        ArrayList<Item> result = new ArrayList<>();

        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ALL_ITEMS)
                .setPriority(Priority.HIGH)
                .setTag("GET_ALL_ITEMS")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONArray items = jsonObject.getJSONArray("content");
                for (int i = 0; i < items.length(); i++) {
                    JSONObject jsonItem = items.getJSONObject(i);
                    Item currentItem = new Item();
                    currentItem.setId(jsonItem.getInt("id"));
                    currentItem.setCategoryId(jsonItem.getInt("categoryId"));
                    //currentItem.setSubCategoryId(jsonItem.getInt("subCategoryId"));

                    //TODO Optimize get directly from server (sum of options voted count)
                    currentItem.setTotalVotes(getItemVotesCount(currentItem.getId()));

                    currentItem.setOptionDescription(Item.OptionChoice.OPTION1,
                            jsonItem.getString("option1Description"));
                    currentItem.setOptionDescription(Item.OptionChoice.OPTION2,
                            jsonItem.getString("option2Description"));

                    currentItem.setOptionImageUrl(Item.OptionChoice.OPTION1, jsonItem.getString(
                            "option1ImageUrl"));

                    currentItem.setOptionImageUrl(Item.OptionChoice.OPTION2, jsonItem.getString(
                            "option2ImageUrl"));

                    result.add(currentItem);
                }
                Collections.sort(result, Collections.reverseOrder());
            } else //Error
                throw new Exception("getAllItems : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("getAllItems : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;

    }

    /**
     * Get an item's comments in the most recent descendant order
     *
     * @return An ArrayList of Comment objects
     * @throws Exception if and error occurred.
     */
    public static ArrayList<Comment> getAllItemComments(int itemId) throws Exception {
        ArrayList<Comment> result = new ArrayList<>();

        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ALL_ITEM_COMMENTS)
                .addQueryParameter("itemId", String.valueOf(itemId))
                .setPriority(Priority.HIGH)
                .setTag("GET_ALL_ITEM_COMMENTS")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONArray comments = jsonObject.getJSONArray("content");
                for (int i = 0; i < comments.length(); i++) {
                    JSONObject jsonComment = comments.getJSONObject(i);
                    Comment currentComment = new Comment();
                    currentComment.setId(jsonComment.getInt("id"))
                            .setItemId(jsonComment.getInt("itemId"))
                            .setUserId(jsonComment.getInt("userId"))
                            .setPostDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jsonComment.getString("date")))
                            .setText(jsonComment.getString("text"))
                            .setReplyToCommentId(jsonComment.isNull("replyToCommentId") ?
                                    COMMENT_NOT_A_REPLY : jsonComment.getInt("replyToCommentId"));

                    result.add(currentComment);
                }
                Collections.sort(result, Collections.reverseOrder());
            } else //Error
                throw new Exception("getAllItemComments : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("getAllItemComments : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;

    }

    /**
     * Get a comment's replies in the most recent descendant order
     *
     * @return An ArrayList of Comment objects
     * @throws Exception if and error occurred.
     */
    public static ArrayList<Comment> getAllCommentReplies(int commentId) throws Exception {
        ArrayList<Comment> result = new ArrayList<>();

        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ALL_COMMENT_REPLIES)
                .addQueryParameter("commentId", String.valueOf(commentId))
                .setPriority(Priority.HIGH)
                .setTag("GET_ALL_COMMENT_REPLIES")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONArray commentReplies = jsonObject.getJSONArray("content");
                for (int i = 0; i < commentReplies.length(); i++) {
                    JSONObject jsonCommentReply = commentReplies.getJSONObject(i);
                    Comment currentCommentReply = new Comment();
                    currentCommentReply.setId(jsonCommentReply.getInt("id"))
                            .setItemId(jsonCommentReply.getInt("itemId"))
                            .setUserId(jsonCommentReply.getInt("userId"))
                            .setPostDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jsonCommentReply.getString("date")))
                            .setText(jsonCommentReply.getString("text"))
                            .setReplyToCommentId(jsonCommentReply.getInt("replyToCommentId"));

                    result.add(currentCommentReply);
                }
                Collections.sort(result, Collections.reverseOrder());
            } else //Error
                throw new Exception("getAllCommentReplies : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("getAllCommentReplies : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;

    }

    /**
     * Vote an item's specific option or remove it
     *
     * @param itemId voted item ID
     * @param option the option voted
     * @param userId user ID
     * @return false if user already voted the other item's option, true if successfully voted or removed vote
     * @throws Exception if and error occurred.
     */
    public static boolean voteItemOption(int itemId, Item.OptionChoice option, int userId) throws Exception {
        int optionVotedNumber = (option == Item.OptionChoice.OPTION1 ? 1 : 2);
        boolean result;
        ANRequest request = AndroidNetworking.post(ApiEndPoint.BASE_URL + ApiEndPoint.VOTE_ITEM_OPTION)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("itemId", String.valueOf(itemId))
                .addBodyParameter("optionVotedNumber", String.valueOf(optionVotedNumber))
                .setPriority(Priority.HIGH)
                .setTag("VOTE_ITEM_OPTION")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
                throw new Exception("voteItemOption : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("voteItemOption : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Like or Dislike a comment
     *
     * @param commentId liked or disliked comment ID
     * @param userId    user ID
     * @return true if successfully liked/disliked or removed like/dislike
     * @throws Exception if and error occurred.
     */
    public static boolean appreciateComment(int commentId, int userId, Comment.CommentAppreciation commentAppreciation) throws Exception {
        int appreciation = (commentAppreciation == Comment.CommentAppreciation.DISLIKE ? -1 : 1);
        boolean result;
        ANRequest request = AndroidNetworking.post(ApiEndPoint.BASE_URL + ApiEndPoint.APPRECIATE_COMMENT)
                .addBodyParameter("userId", String.valueOf(userId))
                .addBodyParameter("commentId", String.valueOf(commentId))
                .addBodyParameter("appreciation", String.valueOf(appreciation))
                .setPriority(Priority.HIGH)
                .setTag("APPRECIATE_COMMENT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
                throw new Exception("appreciateComment : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));

        } else {
            ANError anError = response.getError();
            throw new Exception("appreciateComment : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Get an item's options vote percentages
     *
     * @param itemId item ID
     * @return an {@link HashMap<>} specifying each {@link Float}percentages per {@link Item.OptionChoice}option,
     * the option being the index
     * @throws Exception if and error occurred.
     */
    public static HashMap<Item.OptionChoice, Float> getItemVotePercentages(int itemId) throws Exception {
        HashMap<Item.OptionChoice, Float> result = new HashMap<>();
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ITEM_VOTE_PERCENTAGES)
                .addQueryParameter("itemId", String.valueOf(itemId))
                .setPriority(Priority.HIGH)
                .setTag("GET_ITEM_VOTE_PERCENTAGES")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONObject percentages = jsonObject.getJSONObject("content");
                //TODO Check if any issue relative to getDouble
                result.put(Item.OptionChoice.OPTION1, (float) percentages.getDouble("option1percentage"));
                result.put(Item.OptionChoice.OPTION2, (float) percentages.getDouble(
                        "option2percentage"));
            } else //Error
                throw new Exception("getItemVotePercentages : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
        } else {
            ANError anError = response.getError();
            throw new Exception("getItemVotePercentages : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Get an item's options votes count
     *
     * @param itemId item ID
     * @return an {@link HashMap<>} specifying each {@link Integer}count per {@link Item.OptionChoice}option,
     * the option being the index
     * @throws Exception if and error occurred.
     */
    public static HashMap<Item.OptionChoice, Integer> getItemOptionsVotesCount(int itemId) throws Exception {
        HashMap<Item.OptionChoice, Integer> result = new HashMap<>();
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ITEM_OPTIONS_VOTES_COUNT)
                .addQueryParameter("itemId", String.valueOf(itemId))
                .setPriority(Priority.HIGH)
                .setTag("GET_ITEM_OPTIONS_VOTES_COUNT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS) {
                JSONObject count = jsonObject.getJSONObject("content");
                result.put(Item.OptionChoice.OPTION1, count.getInt("option1VotesCount"));
                result.put(Item.OptionChoice.OPTION2, count.getInt(
                        "option2VotesCount"));
            } else //Error
                throw new Exception("getItemOptionsVotesCount : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
        } else {
            ANError anError = response.getError();
            throw new Exception("getItemOptionsVotesCount : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Get an item's votes total count
     *
     * @param itemId item ID
     * @return specified item's votes total count
     * @throws Exception if and error occurred.
     */
    public static int getItemVotesCount(int itemId) throws Exception {
        int result;
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_ITEM_VOTES_COUNT)
                .addQueryParameter("itemId", String.valueOf(itemId))
                .setPriority(Priority.HIGH)
                .setTag("GET_ITEM_VOTES_COUNT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getInt("content");
            else //Error
                throw new Exception("getItemVotesCount : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
        } else {
            ANError anError = response.getError();
            throw new Exception(" getItemVotesCount: " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Get a comment's replies count
     *
     * @param commentId comment ID
     * @return specified comment's replies count
     * @throws Exception if and error occurred.
     */
    public static int getCommentRepliesCount(int commentId) throws Exception {
        int result;
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_COMMENT_REPLIES_COUNT)
                .addQueryParameter("commentId", String.valueOf(commentId))
                .setPriority(Priority.HIGH)
                .setTag("GET_COMMENT_REPLIES_COUNT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getInt("content");
            else //Error
                throw new Exception("getCommentRepliesCount : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
        } else {
            ANError anError = response.getError();
            throw new Exception("getCommentRepliesCount: " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * Get a comment's appreciation total count
     *
     * @param commentId comment ID
     * @return specified comment's votes total count
     * @throws Exception if and error occurred.
     */
    public static int getCommentAppreciationTotalCount(int commentId, Comment.CommentAppreciation commentAppreciation) throws Exception {
        int appreciation = (commentAppreciation == Comment.CommentAppreciation.DISLIKE ? -1 : 1);
        int result;
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.GET_COMMENT_APPRECIATION_TOTAL_COUNT)
                .addQueryParameter("commentId", String.valueOf(commentId))
                .addQueryParameter("appreciation", String.valueOf(appreciation))
                .setPriority(Priority.HIGH)
                .setTag("GET_COMMENT_APPRECIATION_TOTAL_COUNT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getInt("content");
            else //Error
                throw new Exception("getCommentAppreciationTotalCount : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
        } else {
            ANError anError = response.getError();
            throw new Exception(" getCommentAppreciationTotalCount: " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }

        return result;
    }

    /**
     * @param userId
     * @param itemId
     * @param option
     * @return
     * @throws Exception if and error occurred.
     */
    public static boolean checkUserVotedItemOption(int userId, int itemId, Item.OptionChoice option) throws
            Exception {
        int optionVotedNumber = (option == Item.OptionChoice.OPTION1 ? 1 : 2);
        boolean result;
        ANRequest request = AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.CHECK_USER_VOTED_ITEM_OPTION)
                .addQueryParameter("userId", String.valueOf(userId))
                .addQueryParameter("itemId", String.valueOf(itemId))
                .addQueryParameter("optionVotedNumber", String.valueOf(optionVotedNumber))
                .setPriority(Priority.HIGH)
                .setTag("CHECK_USER_VOTED_ITEM_OPTION")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
            {
                throw new Exception("checkUserVotedItemOption : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
            }

        } else {
            ANError anError = response.getError();
            throw new Exception("checkUserVotedItemOption : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;
    }

    /**
     * Check if a specific user liked or disliked a specific comment
     *
     * @param userId
     * @param commentId
     * @param commentAppreciation
     * @return
     * @throws Exception if and error occurred.
     */
    public static boolean checkUserAppreciatedComment(int userId, int commentId,
                                                      Comment.CommentAppreciation commentAppreciation) throws
            Exception {
        int appreciation = (commentAppreciation == Comment.CommentAppreciation.DISLIKE ? -1 : 1);
        boolean result;
        ANRequest request =
                AndroidNetworking.get(ApiEndPoint.BASE_URL + ApiEndPoint.CHECK_USER_APPRECIATED_COMMENT)
                .addQueryParameter("userId", String.valueOf(userId))
                .addQueryParameter("commentId", String.valueOf(commentId))
                .addQueryParameter("appreciation", String.valueOf(appreciation))
                .setPriority(Priority.HIGH)
                .setTag("CHECK_USER_APPRECIATED_COMMENT")
                .build();
        ANResponse<JSONObject> response = request.executeForJSONObject();
        if (response.isSuccess()) {
            JSONObject jsonObject = response.getResult();
            if (jsonObject.getInt("result") == RESULT_SUCCESS)
                result = jsonObject.getBoolean("content");
            else //Error
            {
                throw new Exception("checkUserAppreciatedComment : " + Objects.requireNonNull(jsonObject.getString(
                        "resultString")));
            }

        } else {
            ANError anError = response.getError();
            throw new Exception("checkUserAppreciatedComment : " + anError.getErrorDetail() + ": " + anError.getMessage() + anError.getErrorBody());
        }
        return result;
    }

}
