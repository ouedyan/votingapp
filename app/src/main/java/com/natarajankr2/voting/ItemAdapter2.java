package com.natarajankr2.voting;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.natarajankr2.voting.model.Category;
import com.natarajankr2.voting.model.Item;
import com.natarajankr2.voting.ui.CrumbleAnimator;
import com.natarajankr2.voting.ui.ParallelogramLayout;
import com.natarajankr2.voting.ui.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

import static com.natarajankr2.voting.model.Item.OptionChoice.OPTION1;
import static com.natarajankr2.voting.model.Item.OptionChoice.OPTION2;
import static com.natarajankr2.voting.ui.Utils.dpToPx;

public class ItemAdapter2 extends RecyclerView.Adapter<ViewHolder> {

    private final ArrayList<ListItem> mListItems;
    private final LinearLayoutManager mLinearLayoutManager;
    private OnItemOptionClickListener onItemOptionClickListener;
    private OnGoToCommentsClick onGoToCommentsClick;

    public ItemAdapter2(ArrayList<ListItem> listItems, LinearLayoutManager linearLayoutManager) {
        this.mListItems = listItems; // reference
        this.mLinearLayoutManager = linearLayoutManager;

        /*mGridLayoutManager.setSpanCount(2);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int dataSize = getItemCount();
                if (dataSize > 0 && position < dataSize - 1) {
                    if (ItemAdapter2.this.mListItems.get(position + 1).getListItemType() == ListItem.TYPE_CATEGORY)
                        if (getItemColumnInGrid(mListItems, position) == 1) //LEFT
                            return mGridLayoutManager.getSpanCount();
                        else return 1;
                }
                return 1;
            }
        });*/
    }

    public interface ListItem {
        int TYPE_CATEGORY = 0;
        int TYPE_ITEM = 1;

        int getListItemType();
    }

    public interface OnItemOptionClickListener {
        void onOptionClick(View optionView, Item item, Item.OptionChoice option, View itemView, int positionInList);
    }

    public interface OnGoToCommentsClick {
        void onGoToCommentsClick(Item item);
    }

    public void setOnItemOptionClickListener(OnItemOptionClickListener onItemOptionClickListener) {
        this.onItemOptionClickListener = onItemOptionClickListener;
    }

    public void setOnGoToCommentsClick(OnGoToCommentsClick onGoToCommentsClick) {
        this.onGoToCommentsClick = onGoToCommentsClick;
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems.get(position).getListItemType();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ListItem.TYPE_CATEGORY:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout2, parent,
                        false);
                return new CategoryViewHolder(itemView);
            case ListItem.TYPE_ITEM:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout2, parent, false);
                return new ItemViewHolder(itemView);
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        double textRotationRadians = Math.atan((double) mLinearLayoutManager.getWidth() / (dpToPx(holder.itemView.getResources(), (int) (0.3 * 200))));

        float textRotationDegrees = (float) -Math.toDegrees(textRotationRadians);
        textRotationDegrees = -90 - textRotationDegrees; // atan () result is in range -pi/2
        // through pi/2 ; need result in range 0 through pi/2

        switch (getItemViewType(position)) {

            case ListItem.TYPE_CATEGORY:
                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
                Category currentCategory = (Category) mListItems.get(position);

                Glide.with(holder.itemView.getContext())
                        .load(currentCategory.getImageUrl())
                        .thumbnail(0.1f)
                        .placeholder(R.color.aboveOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<ParallelogramLayout, Drawable>(categoryViewHolder.categoryLayout) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                categoryViewHolder.categoryLayout.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                categoryViewHolder.categoryLayout.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });


                categoryViewHolder.categoryName.setText(currentCategory.getName());
                categoryViewHolder.categoryName.setRotation(textRotationDegrees);

                //If not 1st listed category : android:layout_marginTop="-30dp"
                if (position > 0)
                    ((ViewGroup.MarginLayoutParams) categoryViewHolder.categoryLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -30), 0, 0);
                else
                    ((ViewGroup.MarginLayoutParams) categoryViewHolder.categoryLayout.getLayoutParams()).setMargins(0, 0, 0, 0);


                break;

            case ListItem.TYPE_ITEM:
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                Item currentItem = (Item) mListItems.get(position);
                Item.OptionChoice optionLoggedInUserVoted = currentItem.getOptionLoggedInUserVoted();

                //Position options
                if (optionLoggedInUserVoted == null) {
                    //If nothing voted, most voted option above
                    itemViewHolder.resetConstraintsToPutOptionAbove(currentItem.getMostVotedOption());

                    //TODO Remove any stroke shadowed color
                    //itemViewHolder.option1.setShadow(null);
                    //itemViewHolder.option2.setShadow(null);
                    switch (itemViewHolder.getAboveOption()) {
                        case OPTION1:
                            itemViewHolder.option1.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.aboveOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            itemViewHolder.option2.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.belowOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            break;
                        case OPTION2:
                            itemViewHolder.option2.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.aboveOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            itemViewHolder.option1.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.belowOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            break;
                    }

                } else {
                    itemViewHolder.resetConstraintsToPutOptionAbove(optionLoggedInUserVoted);

                    switch (optionLoggedInUserVoted) {
                        case OPTION1:
                            //TODO Push down and back click reaction
                            //TODO set stroke shadowed color or Background instead
                            //itemViewHolder.option1.setShadow()
                            itemViewHolder.option1.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.votedOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            itemViewHolder.option2.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.belowOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            break;
                        case OPTION2:
                            //TODO Push down and back click reaction
                            //TODO set stroke shadowed color or Background instead
                            //itemViewHolder.option2.setShadow()
                            itemViewHolder.option2.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.votedOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            itemViewHolder.option1.setBackgroundColor(ResourcesCompat.getColor(itemViewHolder.itemView.getResources(), R.color.belowOptionBackgroundColor, itemViewHolder.itemView.getContext().getTheme()));
                            break;
                    }
                }


                Glide.with(holder.itemView.getContext())
                        .load(currentItem.getOptionImageUrl(OPTION2))
                        .thumbnail(0.1f)
                        .placeholder(optionLoggedInUserVoted == OPTION1 ? R.color.belowOptionBackgroundColor :
                                R.color.aboveOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<ParallelogramLayout, Drawable>(itemViewHolder.option2Image) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                itemViewHolder.option2Image.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                itemViewHolder.option2Image.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });

                Glide.with(holder.itemView.getContext())
                        .load(currentItem.getOptionImageUrl(OPTION1))
                        .thumbnail(0.1f)
                        .placeholder(optionLoggedInUserVoted == OPTION1 ? R.color.aboveOptionBackgroundColor :
                                R.color.belowOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<ParallelogramLayout, Drawable>(itemViewHolder.option1Image) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                itemViewHolder.option1Image.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                itemViewHolder.option1Image.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });


                itemViewHolder.option2Description.setText(currentItem.getOptionDescription(OPTION2));
                itemViewHolder.option1Description.setText(currentItem.getOptionDescription(OPTION1));
                /*itemViewHolder.option2VotesCountText.setText(
                        itemViewHolder.itemView.getResources().getQuantityString(
                                R.plurals.votes_count_text,
                                currentItem.getOptionVotesCount(Item.OptionChoice.OPTION2),
                                currentItem.getOptionVotesCount(Item.OptionChoice.OPTION2)));
                itemViewHolder.option1VotesCountText.setText(
                        itemViewHolder.itemView.getResources().getQuantityString(
                                R.plurals.votes_count_text,
                                currentItem.getOptionVotesCount(Item.OptionChoice.OPTION1),
                                currentItem.getOptionVotesCount(Item.OptionChoice.OPTION1)));*/
                itemViewHolder.option2VotesPercentageText.setText((int) currentItem.getOptionVotesPercentage(OPTION2) + " %");
                itemViewHolder.option1VotesPercentageText.setText((int) currentItem.getOptionVotesPercentage(OPTION1) + " %");

                itemViewHolder.option2Description.setRotation(textRotationDegrees);
                itemViewHolder.option1Description.setRotation(textRotationDegrees);
                itemViewHolder.option2VotesPercentageText.setRotation(textRotationDegrees);
                itemViewHolder.option1VotesPercentageText.setRotation(textRotationDegrees);
                itemViewHolder.option2CommentsButton.setRotation(textRotationDegrees);
                itemViewHolder.option1CommentsButton.setRotation(textRotationDegrees);
                itemViewHolder.option2VotesImage.setRotation(textRotationDegrees);
                itemViewHolder.option1VotesImage.setRotation(textRotationDegrees);
                itemViewHolder.punchGif.setRotation(textRotationDegrees);

                //Punch Animation settings
                //GifDrawable gif = (GifDrawable) itemViewHolder.punch.getDrawable();
                itemViewHolder.punchGif.setVisibility(View.GONE);

                itemViewHolder.option2.setTranslationX(0);
                itemViewHolder.option2.setTranslationY(0);
                itemViewHolder.option1.setTranslationX(0);
                itemViewHolder.option1.setTranslationY(0);

                //If listed item cell number not odd -is to right- listed category : android:layout_marginTop="-30dp"
                /*if (getItemColumnInGrid(new ArrayList<>(mListItems), position) == 0) //RIGHT COLUMN
                    if (position == 1)
                        ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -30), 0, 0);
                    else
                        ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -90), 0, 0);
                else //LEFT COLUMN
                    ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0,
                            dpToPx(holder.itemView.getResources(), -30), 0, 0);*/
                //boolean isPrecedentListItemCategory =
                //mListItems.get(position - 1).getListItemType() == ListItem.TYPE_CATEGORY ;
                //int marginTop = isPrecedentListItemCategory ? -60 : -30 ;
                ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0,
                        dpToPx(holder.itemView.getResources(), -60), 0, 0);

                View.OnClickListener onOption2ClickListener = optionView ->
                        ItemAdapter2.this.onItemOptionClickListener.onOptionClick(
                                optionView,
                                currentItem,
                                OPTION2,
                                itemViewHolder.itemView,
                                position);

                View.OnClickListener onOption1ClickListener = optionView ->
                        ItemAdapter2.this.onItemOptionClickListener.onOptionClick(
                                optionView,
                                currentItem,
                                OPTION1,
                                itemViewHolder.itemView,
                                position);

                View.OnClickListener onCommentsButtonClickListener = (view) ->
                        onGoToCommentsClick.onGoToCommentsClick(currentItem);

                itemViewHolder.option2CommentsButton.setOnClickListener(onCommentsButtonClickListener);
                itemViewHolder.option1CommentsButton.setOnClickListener(onCommentsButtonClickListener);
                itemViewHolder.itemView.findViewById(R.id.option2_image_black_layer).setOnClickListener(onOption2ClickListener);
                itemViewHolder.itemView.findViewById(R.id.option1_image_black_layer).setOnClickListener(onOption1ClickListener);


                break;

            default:
                throw new IllegalStateException("Unexpected value: " + getItemViewType(position));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (!payloads.isEmpty()) {

            switch (getItemViewType(position)) {

                case ListItem.TYPE_CATEGORY:
                    super.onBindViewHolder(holder, position, payloads);
                    break;

                case ListItem.TYPE_ITEM:
                    HashMap<String, HashMap<String, Object>> mapsOfPayloads = (HashMap<String, HashMap<String, Object>>) payloads.get(0);
                    if (mapsOfPayloads.containsKey("optionVotedPayload")) {
                        HashMap<String, Object> optionVotedPayload = mapsOfPayloads.get("optionVotedPayload");

                        Item.OptionChoice concernedOption = (Item.OptionChoice) Objects.requireNonNull(optionVotedPayload).get(
                                "concernedOption");
                        //boolean cancellation = (boolean) Objects.requireNonNull(optionVotedPayload.get("cancellation"));

                        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                        Item currentItem = (Item) mListItems.get(position);

                        boolean cancellation = !currentItem.loggedUserVoted(concernedOption);

                        itemViewHolder.reactToVoteClick(currentItem, cancellation);


                       /*boolean isPunchDown = !currentItem.loggedUserVoted(itemViewHolder.getBelowOption())
                                && (currentItem.getMostVotedOption() != itemViewHolder.getBelowOption());
                        itemViewHolder.punch(isPunchDown,
                                currentItem.getOptionVotesPercentage(itemViewHolder.getAboveOption()),
                                currentItem.getOptionVotesPercentage(itemViewHolder.getBelowOption()));*/

                        if (currentItem.loggedUserVoted(itemViewHolder.getBelowOption())
                                || (currentItem.getMostVotedOption() == itemViewHolder.getBelowOption())) {
                            itemViewHolder.punch(false,
                                    currentItem.getOptionVotesPercentage(itemViewHolder.getAboveOption()),
                                    currentItem.getOptionVotesPercentage(itemViewHolder.getBelowOption()));

                        } else if (currentItem.loggedUserVoted(itemViewHolder.getAboveOption())) {
                            itemViewHolder.punch(true,
                                    currentItem.getOptionVotesPercentage(itemViewHolder.getBelowOption()),
                                    currentItem.getOptionVotesPercentage(itemViewHolder.getAboveOption()));
                        } else { //Vote cancellation
                            itemViewHolder.unVote(concernedOption,
                                    currentItem.getOptionVotesPercentage(Objects.requireNonNull(concernedOption)),
                                    currentItem.getOptionVotesPercentage(Objects.requireNonNull(concernedOption) == OPTION1 ? OPTION2 : OPTION1));

                        }

                    }

                    break;
            }
        } else
            super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    static class ItemViewHolder extends ViewHolder {
        ConstraintLayout itemLayout;
        ParallelogramLayout option2;
        ParallelogramLayout option1;
        ParallelogramLayout option2Image;
        ParallelogramLayout option1Image;
        TextView option2Description;
        TextView option1Description;
        TextView option2VotesPercentageText;
        TextView option1VotesPercentageText;
        ImageView option2CommentsButton;
        ImageView option1CommentsButton;
        ImageView option2VotesImage;
        ImageView option1VotesImage;
        GifImageView punchGif;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemLayout = (ConstraintLayout) itemView;
            option2 = itemView.findViewById(R.id.option2);
            option1 = itemView.findViewById(R.id.option1);
            option2Image = itemView.findViewById(R.id.option2_image);
            option1Image = itemView.findViewById(R.id.option1_image);
            option2Description = itemView.findViewById(R.id.option2_description);
            option1Description = itemView.findViewById(R.id.option1_description);
            option2VotesPercentageText = itemView.findViewById(R.id.option2_votes_percentage_text);
            option1VotesPercentageText = itemView.findViewById(R.id.option1_votes_percentage_text);
            option2CommentsButton = itemView.findViewById(R.id.option2_comments_button);
            option1CommentsButton = itemView.findViewById(R.id.option1_comments_button);
            option2VotesImage = itemView.findViewById(R.id.option2_votes_count_image);
            option1VotesImage = itemView.findViewById(R.id.option1_votes_count_image);
            punchGif = itemView.findViewById(R.id.punchGif);

        }

        public boolean resetConstraintsToPutOptionAbove(Item.OptionChoice option) {
            /*ConstraintSet itemConstraintSet = new ConstraintSet();
            itemConstraintSet.clone(itemLayout);
            itemConstraintSet.clear(option1.getId());
            itemConstraintSet.clear(option2.getId());
            itemConstraintSet.applyTo(itemLayout);*/

            if (option == getAboveOption())
                return false;

            ConstraintLayout.LayoutParams option1LayoutParams = (ConstraintLayout.LayoutParams) option1.getLayoutParams();
            ConstraintLayout.LayoutParams option2LayoutParams = (ConstraintLayout.LayoutParams) option2.getLayoutParams();

            switch (option) {
                case OPTION1:
                    option2LayoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
                    option2LayoutParams.endToStart = R.id.option1;
                    option2LayoutParams.endToEnd = ConstraintLayout.LayoutParams.UNSET;
                    option2LayoutParams.startToEnd = ConstraintLayout.LayoutParams.UNSET;
                    option2LayoutParams.topMargin = dpToPx(itemView.getResources(), 30);

                    option1LayoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
                    option1LayoutParams.startToEnd = R.id.option2;
                    option1LayoutParams.startToStart = ConstraintLayout.LayoutParams.UNSET;
                    option1LayoutParams.endToStart = ConstraintLayout.LayoutParams.UNSET;
                    option1LayoutParams.topMargin = 0;
                    break;
                case OPTION2:
                    option1LayoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
                    option1LayoutParams.endToStart = R.id.option2;
                    option1LayoutParams.endToEnd = ConstraintLayout.LayoutParams.UNSET;
                    option1LayoutParams.startToEnd = ConstraintLayout.LayoutParams.UNSET;
                    option1LayoutParams.topMargin = dpToPx(itemView.getResources(), 30);

                    option2LayoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
                    option2LayoutParams.startToEnd = R.id.option1;
                    option2LayoutParams.startToStart = ConstraintLayout.LayoutParams.UNSET;
                    option2LayoutParams.endToStart = ConstraintLayout.LayoutParams.UNSET;
                    option2LayoutParams.topMargin = 0;
                    break;
            }
            option1.setLayoutParams(option1LayoutParams);
            option2.setLayoutParams(option2LayoutParams);

            return true;
        }

        public Item.OptionChoice getAboveOption() {
            ConstraintLayout.LayoutParams option1LayoutParams = (ConstraintLayout.LayoutParams) option1.getLayoutParams();
            ConstraintLayout.LayoutParams option2LayoutParams = (ConstraintLayout.LayoutParams) option2.getLayoutParams();

            /*if (option1LayoutParams.startToStart == ConstraintLayout.LayoutParams.UNSET && option2LayoutParams.endToEnd ==
            ConstraintLayout.LayoutParams.UNSET)
                return null;*/
            if (option1LayoutParams.startToStart == ConstraintLayout.LayoutParams.PARENT_ID && option2LayoutParams.endToEnd == ConstraintLayout.LayoutParams.PARENT_ID)
                return OPTION2;
            else if (option2LayoutParams.startToStart == ConstraintLayout.LayoutParams.PARENT_ID && option1LayoutParams.endToEnd == ConstraintLayout.LayoutParams.PARENT_ID)
                return OPTION1;
            else
                return null;
        }

        public Item.OptionChoice getBelowOption() {
            if (getAboveOption() == null) return null;
            return getAboveOption() == OPTION1 ? OPTION2 : OPTION1;
        }

        public void reactToVoteClick(Item currentItem, boolean cancellation) {
            if (!cancellation) {
                //TODO set stroke shadowed color or Background instead
                //votedOptionView.setShadow(coloredShadow)
                switch (currentItem.getOptionLoggedInUserVoted()) {
                    case OPTION1:
                        //TODO set stroke shadowed color
                        //itemViewHolder.option1.setShadow()
                        option1.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.votedOptionBackgroundColor, itemView.getContext().getTheme()));
                        option2.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.belowOptionBackgroundColor, itemView.getContext().getTheme()));
                        break;
                    case OPTION2:
                        //TODO set stroke shadowed color
                        //itemViewHolder.option2.setShadow()
                        option2.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.votedOptionBackgroundColor, itemView.getContext().getTheme()));
                        option1.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.belowOptionBackgroundColor, itemView.getContext().getTheme()));
                        break;
                }

            } else {
                //TODO set stroke shadowed color or Background instead
                //votedOptionView.setShadow(null)
                switch (currentItem.getMostVotedOption()) {
                    case OPTION1:
                        option1.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.aboveOptionBackgroundColor, itemView.getContext().getTheme()));
                        option2.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.belowOptionBackgroundColor, itemView.getContext().getTheme()));
                        break;
                    case OPTION2:
                        option2.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.aboveOptionBackgroundColor, itemView.getContext().getTheme()));
                        option1.setBackgroundColor(ResourcesCompat.getColor(itemView.getResources(), R.color.belowOptionBackgroundColor, itemView.getContext().getTheme()));
                        break;
                }
            }
        }

        public void punch(boolean isPunchDown, float futureBelowOptionVotesPercentage, float futureAboveOptionVotesPercentage) {
            //Animations
            ParallelogramLayout aboveOption = getAboveOption() == OPTION1 ? option1 : option2;
            ParallelogramLayout belowOption = getBelowOption() == OPTION1 ? option1 : option2;
            TextView aboveOptionVotesPercentageView = getAboveOption() == OPTION1 ?
                    option1VotesPercentageText : option2VotesPercentageText;
            TextView belowOptionVotesPercentageView = getBelowOption() == OPTION1 ?
                    option1VotesPercentageText : option2VotesPercentageText;

            float verticalOffset = Utils.dpToPx(itemView.getResources(), 30);

            if (isPunchDown) {
                //Crumbling
                CrumbleAnimator crumbleAnimator = CrumbleAnimator.get(belowOptionVotesPercentageView,
                        new CrumbleAnimator.Config()
                                .setHideOriginalViewOnAnimation(false)
                                .setBreakDuration(300)
                                .setComplexity(20)
                                .setCircleRiftsRadius(66)
                                .setFallDuration(2000));
                crumbleAnimator.setOnCrumbleListener(new CrumbleAnimator.OnCrumbleListener() {
                    @Override
                    public void onStart(View v) {
                    }

                    @Override
                    public void onCancel(View v) {

                    }

                    @Override
                    public void onRestart(View v) {

                    }

                    @Override
                    public void onFalling(View v) {
                        //Start current below option's votes percentage joy animation before change
                        belowOptionVotesPercentageView.setText((int) futureBelowOptionVotesPercentage + " %");
                        aboveOptionVotesPercentageView.setTextColor(Color.GREEN);
                        YoYo.with(Techniques.RubberBand)
                                .duration(700)
                                .withListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        aboveOptionVotesPercentageView.setText((int) futureAboveOptionVotesPercentage + " %");
                                        YoYo.with(Techniques.Tada)
                                                .duration(700)
                                                .withListener(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        aboveOptionVotesPercentageView.setTextColor(Color.WHITE);
                                                        aboveOptionVotesPercentageView.setRotation(belowOptionVotesPercentageView.getRotation());
                                                    }
                                                })
                                                .playOn(aboveOptionVotesPercentageView);
                                    }
                                })
                                .playOn(aboveOptionVotesPercentageView);
                    }

                    @Override
                    public void onFallingEnd(View v) {
                        punchGif.setRotationY(0);
                        punchGif.setVisibility(View.GONE);

                    }

                    @Override
                    public void onCancelEnd(View v) {

                    }
                });

                //Shake after shock
                ArrayList<Animator> shakeAnimations = new ArrayList<>();
                for (float translation = 0.3f; translation > 0; translation -= 0.1f) {

                    PropertyValuesHolder translationX =
                            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0,
                                    -belowOption.getWidth() * translation, 0, belowOption.getWidth() * translation, 0);
                    PropertyValuesHolder translationY =
                            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0,
                                    verticalOffset * translation, 0, -verticalOffset * translation, 0);
                    ValueAnimator shakeAnimation =
                            ObjectAnimator.ofPropertyValuesHolder(belowOption,
                                    translationX,
                                    translationY)
                                    .setDuration((long) (1000 * translation * 2));

                    shakeAnimations.add(shakeAnimation);
                }
                AnimatorSet shakeAnimator = new AnimatorSet();
                shakeAnimator.playSequentially(shakeAnimations);
                shakeAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        crumbleAnimator.start();
                    }
                });

                punchGif.setRotationY(180);
                GifDrawable punchGifDrawable = (GifDrawable) punchGif.getDrawable();
                //gif.setVisible(true, true);
                punchGif.setVisibility(View.VISIBLE);
                punchGifDrawable.reset(); //Launch animation

                new Handler(Looper.getMainLooper()).postDelayed(shakeAnimator::start, punchGifDrawable.getDuration() / 2);

            } else {//Punch up

                //3 Slides

                PropertyValuesHolder translationXAboveOption =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0,
                                aboveOption.getWidth());
                PropertyValuesHolder translationYAboveOption =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0,
                                -verticalOffset);
                ValueAnimator slideAnimation1 =
                        ObjectAnimator.ofPropertyValuesHolder(aboveOption,
                                translationXAboveOption,
                                translationYAboveOption)
                                .setDuration(1000);

                PropertyValuesHolder translationXBelowOption =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0,
                                belowOption.getWidth());
                PropertyValuesHolder translationYBelowOption =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0,
                                -verticalOffset);
                ValueAnimator slideAnimation2 =
                        ObjectAnimator.ofPropertyValuesHolder(belowOption,
                                translationXBelowOption,
                                translationYBelowOption)
                                .setDuration(1500);

                PropertyValuesHolder translationXAboveOption2 =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_X, -aboveOption.getWidth() * 2,
                                -aboveOption.getWidth());
                PropertyValuesHolder translationYAboveOption2 =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_Y,
                                verticalOffset * 2, verticalOffset);
                ValueAnimator slideAnimation3 =
                        ObjectAnimator.ofPropertyValuesHolder(aboveOption,
                                translationXAboveOption2
                                , translationYAboveOption2)
                                .setDuration(1500);

                Runnable setNewAboveOptionAnims = () -> {
                    punchGif.setVisibility(View.GONE);
                    //Move below option up an above option take the place left by
                    // below option
                    AnimatorSet animator = new AnimatorSet();
                    animator.playSequentially(slideAnimation1, slideAnimation2, slideAnimation3);
                    animator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            //Bring back translations to normal after animation and reposition options
                            aboveOption.setTranslationX(0);
                            aboveOption.setTranslationY(0);
                            belowOption.setTranslationX(0);
                            belowOption.setTranslationY(0);
                            resetConstraintsToPutOptionAbove(getBelowOption());

                        }
                    });
                    animator.start();
                };

                //

                //Crumbling
                CrumbleAnimator crumbleAnimator = CrumbleAnimator.get(aboveOptionVotesPercentageView,
                        new CrumbleAnimator.Config()
                                .setHideOriginalViewOnAnimation(false)
                                .setBreakDuration(300)
                                .setComplexity(20)
                                .setCircleRiftsRadius(66)
                                .setFallDuration(2000));
                crumbleAnimator.setOnCrumbleListener(new CrumbleAnimator.OnCrumbleListener() {
                    @Override
                    public void onStart(View v) {
                    }

                    @Override
                    public void onCancel(View v) {

                    }

                    @Override
                    public void onRestart(View v) {

                    }

                    @Override
                    public void onFalling(View v) {
                        //Start current below option's votes percentage joy animation before change
                        aboveOptionVotesPercentageView.setText((int) futureBelowOptionVotesPercentage + " %");
                        belowOptionVotesPercentageView.setTextColor(Color.GREEN);
                        YoYo.with(Techniques.RubberBand)
                                .duration(700)
                                .withListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        belowOptionVotesPercentageView.setText((int) futureAboveOptionVotesPercentage + " %");
                                        YoYo.with(Techniques.Tada)
                                                .duration(700)
                                                .withListener(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        belowOptionVotesPercentageView.setTextColor(Color.WHITE);
                                                        belowOptionVotesPercentageView.setRotation(aboveOptionVotesPercentageView.getRotation());
                                                    }
                                                })
                                                .playOn(belowOptionVotesPercentageView);
                                    }
                                })
                                .playOn(belowOptionVotesPercentageView);
                    }

                    @Override
                    public void onFallingEnd(View v) {
                        //Remove above option
                        new Handler(Looper.getMainLooper()).post(setNewAboveOptionAnims);
                    }

                    @Override
                    public void onCancelEnd(View v) {

                    }
                });

                //Shake after shock
                ArrayList<Animator> shakeAnimations = new ArrayList<>();
                for (float translation = 0.3f; translation > 0; translation -= 0.1f) {

                    PropertyValuesHolder translationX =
                            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0,
                                    aboveOption.getWidth() * translation, 0, -aboveOption.getWidth() * translation, 0);
                    PropertyValuesHolder translationY =
                            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0,
                                    -verticalOffset * translation, 0, verticalOffset * translation, 0);
                    ValueAnimator shakeAnimation =
                            ObjectAnimator.ofPropertyValuesHolder(aboveOption,
                                    translationX,
                                    translationY)
                                    .setDuration((long) (1000 * translation * 2));

                /*PropertyValuesHolder translationXBackward =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0,
                                -aboveOption.getWidth() * translation);
                PropertyValuesHolder translationYBackward =
                        PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0,
                                verticalOffset * translation);
                ValueAnimator shakeAnimationBackward =
                        ObjectAnimator.ofPropertyValuesHolder(aboveOption,
                                translationXBackward,
                                translationYBackward)
                                .setDuration((long)(1000 * translation));*/

                    shakeAnimations.add(shakeAnimation);
                }
                AnimatorSet shakeAnimator = new AnimatorSet();
                shakeAnimator.playSequentially(shakeAnimations);
                shakeAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        crumbleAnimator.start();
                    }
                });

                GifDrawable punchGifDrawable = (GifDrawable) punchGif.getDrawable();
                //gif.setVisible(true, true);
                punchGif.setVisibility(View.VISIBLE);
                punchGifDrawable.reset(); //Launch animation

                new Handler(Looper.getMainLooper()).postDelayed(shakeAnimator::start, punchGifDrawable.getDuration() / 2);

            }
        }

        public void unVote(Item.OptionChoice option, float unVotedOptionFutureVotesPercentage,
                           float theOtherOptionFutureVotesPercentage){
            TextView unVotedOptionVotesPercentageView = option == OPTION1 ?
                    option1VotesPercentageText : option2VotesPercentageText;
            TextView theOtherOptionVotesPercentageView = option == OPTION1 ?
                    option2VotesPercentageText : option1VotesPercentageText;

            //Crumbling
            CrumbleAnimator crumbleAnimator = CrumbleAnimator.get(unVotedOptionVotesPercentageView,
                    new CrumbleAnimator.Config()
                            .setHideOriginalViewOnAnimation(false)
                            .setBreakDuration(300)
                            .setComplexity(20)
                            .setCircleRiftsRadius(66)
                            .setFallDuration(2000));
            crumbleAnimator.setOnCrumbleListener(new CrumbleAnimator.OnCrumbleListener() {
                @Override
                public void onStart(View v) {
                }

                @Override
                public void onCancel(View v) {

                }

                @Override
                public void onRestart(View v) {

                }

                @Override
                public void onFalling(View v) {
                    unVotedOptionVotesPercentageView.setText((int) unVotedOptionFutureVotesPercentage + " %");
                    //Start the other option's votes percentage joy animation before change
                    theOtherOptionVotesPercentageView.setTextColor(Color.GREEN);
                    YoYo.with(Techniques.RubberBand)
                            .duration(700)
                            .withListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    theOtherOptionVotesPercentageView.setText((int) theOtherOptionFutureVotesPercentage + " %");
                                    YoYo.with(Techniques.Tada)
                                            .duration(700)
                                            .withListener(new AnimatorListenerAdapter() {
                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    theOtherOptionVotesPercentageView.setTextColor(Color.WHITE);
                                                    theOtherOptionVotesPercentageView.setRotation(unVotedOptionVotesPercentageView.getRotation());
                                                }
                                            })
                                            .playOn(theOtherOptionVotesPercentageView);
                                }
                            })
                            .playOn(theOtherOptionVotesPercentageView);
                }

                @Override
                public void onFallingEnd(View v) {

                }

                @Override
                public void onCancelEnd(View v) {

                }
            });

            crumbleAnimator.start();

        }

    }

    static class CategoryViewHolder extends ViewHolder {
        ParallelogramLayout categoryLayout;
        TextView categoryName;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryLayout = (ParallelogramLayout) itemView;
            categoryName = itemView.findViewById(R.id.category_name);
        }

    }

    /*private int getItemColumnInGrid(ArrayList<ListItem> listItems, int itemPosition) {
        int count = 0;
        do {
            count++;
        } while (listItems.get(itemPosition - count).getListItemType() != ListItem.TYPE_CATEGORY);

        return 1 - count % 2;

    }*/
}
