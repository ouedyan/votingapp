package com.natarajankr2.voting;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.natarajankr2.voting.model.Category;
import com.natarajankr2.voting.model.Item;
import com.natarajankr2.voting.ui.CategoryConstraintLayout;
import com.natarajankr2.voting.ui.LessVotedButton;
import com.natarajankr2.voting.ui.MostVotedButton;
import com.natarajankr2.voting.ui.Utils;

import java.util.ArrayList;

import static com.natarajankr2.voting.ui.Utils.dpToPx;

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<ListItem> mListItems;
    private final GridLayoutManager mGridLayoutManager;
    private OnItemOptionClickListener onItemOptionClickListener;
    private OnItemDoubleClickListener onItemDoubleClickListener;

    public ItemAdapter(ArrayList<ListItem> listItems, GridLayoutManager gridLayoutManager) {
        this.mListItems = listItems; // reference
        this.mGridLayoutManager = gridLayoutManager;

        mGridLayoutManager.setSpanCount(2);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int dataSize = getItemCount();
                if (dataSize > 0 && position < dataSize - 1) {
                    if (ItemAdapter.this.mListItems.get(position + 1).getListItemType() == ListItem.TYPE_CATEGORY)
                        if (getItemColumnInGrid(mListItems, position) == 1) //LEFT
                            return mGridLayoutManager.getSpanCount();
                        else return 1;
                }
                return 1;
            }
        });
    }

    public abstract static class ListItem {
        public static final int TYPE_CATEGORY = 0;
        public static final int TYPE_ITEM = 1;

        public abstract int getListItemType();
    }

    public interface OnItemOptionClickListener {
        void onOptionClick(View optionView, Item item, Item.OptionChoice option, int position);
    }

    public interface OnItemDoubleClickListener {
        void onDoubleClick(Item item);
    }

    public void setOnItemOptionClickListener(OnItemOptionClickListener onItemOptionClickListener) {
        this.onItemOptionClickListener = onItemOptionClickListener;
    }

    public void setOnItemDoubleClickListener(OnItemDoubleClickListener onItemDoubleClickListener) {
        this.onItemDoubleClickListener = onItemDoubleClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems.get(position).getListItemType();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ListItem.TYPE_CATEGORY:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout, parent, false);
                return new CategoryViewHolder(itemView);
            case ListItem.TYPE_ITEM:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
                return new ItemViewHolder(itemView);
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        double textRotationRadiansCategories = Math.atan((double) mGridLayoutManager.getWidth() / (dpToPx(holder.itemView.getResources(), (int) (0.3 * 200))));
        double textRotationRadiansItems = Math.atan(((double) mGridLayoutManager.getWidth() / 2) / (dpToPx(holder.itemView.getResources(), (int) (0.85 * 200))));

        float textRotationDegreesCategories = (float) -Math.toDegrees(textRotationRadiansCategories);
        textRotationDegreesCategories = -90 - textRotationDegreesCategories; // atan () result is in range -pi/2
        // through pi/2 ; need result in range 0 through pi/2

        float textRotationDegreesItems = (float) -Math.toDegrees(textRotationRadiansItems);
        textRotationDegreesItems = -90 - textRotationDegreesItems; // atan () result is in range -pi/2 through
        // pi/2 ; need result in range 0 through pi/2
        switch (getItemViewType(position)) {

            case ListItem.TYPE_CATEGORY:
                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
                Category currentCategory = (Category) mListItems.get(position);

                Glide.with(holder.itemView.getContext())
                        .load(currentCategory.getImageUrl())
                        .thumbnail(0.1f)
                        .placeholder(R.color.aboveOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<ConstraintLayout, Drawable>(categoryViewHolder.categoryLayout) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                categoryViewHolder.categoryLayout.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                categoryViewHolder.categoryLayout.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });


                categoryViewHolder.categoryName.setText(currentCategory.getName());
                categoryViewHolder.categoryName.setRotation(textRotationDegreesCategories);

                //If not 1st listed category : android:layout_marginTop="-60dp"
                if (position > 0)
                    ((ViewGroup.MarginLayoutParams) categoryViewHolder.categoryLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -60), 0, 0);
                else
                    ((ViewGroup.MarginLayoutParams) categoryViewHolder.categoryLayout.getLayoutParams()).setMargins(0, 0, 0, 0);
                break;

            case ListItem.TYPE_ITEM:
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                Item currentItem = (Item) mListItems.get(position);
                Item.OptionChoice mostVotedOption = currentItem.getMostVotedOption();
                Item.OptionChoice lessVotedOption = currentItem.getLessVotedOption();

                Glide.with(holder.itemView.getContext())
                        .load(currentItem.getOptionImageUrl(mostVotedOption))
                        .thumbnail(0.1f)
                        .placeholder(R.color.aboveOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<MostVotedButton, Drawable>(itemViewHolder.mostVotedOption) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                itemViewHolder.mostVotedOption.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                itemViewHolder.mostVotedOption.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });

                Glide.with(holder.itemView.getContext())
                        .load(currentItem.getOptionImageUrl(lessVotedOption))
                        .thumbnail(0.1f)
                        .placeholder(R.color.belowOptionBackgroundColor)
                        .error(R.drawable.ic_broken_image_24)
                        .into(new CustomViewTarget<LessVotedButton, Drawable>(itemViewHolder.lessVotedOption) {
                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                itemViewHolder.lessVotedOption.setBackground(errorDrawable);
                            }

                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                itemViewHolder.lessVotedOption.setBackground(resource);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }
                        });


                itemViewHolder.mostVotedOptDescription.setText(currentItem.getOptionDescription(mostVotedOption));
                itemViewHolder.lessVotedOptDescription.setText(currentItem.getOptionDescription(lessVotedOption));
                itemViewHolder.mostVotedOptPercentage.setText((int) currentItem.getOptionVotesPercentage(mostVotedOption) + "%");
                itemViewHolder.lessVotedOptPercentage.setText((int) currentItem.getOptionVotesPercentage(lessVotedOption) + "%");
                itemViewHolder.mostVotedOptDescription.setRotation(textRotationDegreesItems);
                itemViewHolder.lessVotedOptDescription.setRotation(textRotationDegreesItems);
                itemViewHolder.mostVotedOptPercentage.setRotation(textRotationDegreesCategories);
                itemViewHolder.lessVotedOptPercentage.setRotation(textRotationDegreesCategories);
                itemViewHolder.mostVotedVoteIcon.setRotation(textRotationDegreesCategories);
                itemViewHolder.lessVotedVoteIcon.setRotation(textRotationDegreesCategories);

                if (((Item) mListItems.get(position)).loggedUserVoted(mostVotedOption)) {
                    itemViewHolder.mostVotedVoteIcon.setImageResource(R.drawable.ic_voted);
                    itemViewHolder.lessVotedVoteIcon.setImageResource(R.drawable.ic_non_voted);
                } else if (((Item) mListItems.get(position)).loggedUserVoted(lessVotedOption)) {
                    itemViewHolder.lessVotedVoteIcon.setImageResource(R.drawable.ic_voted);
                    itemViewHolder.mostVotedVoteIcon.setImageResource(R.drawable.ic_non_voted);
                } else {
                    itemViewHolder.mostVotedVoteIcon.setImageResource(R.drawable.ic_non_voted);
                    itemViewHolder.lessVotedVoteIcon.setImageResource(R.drawable.ic_non_voted);
                }

                itemViewHolder.itemLayout.setMaxWidth(mGridLayoutManager.getWidth() / 2);

                //If listed item cell number not odd -is to right- listed category : android:layout_marginTop="-60dp"
                if (getItemColumnInGrid(new ArrayList<>(mListItems), position) == 0) //RIGHT COLUMN
                    if (position == 1)
                        ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -30), 0, 0);
                    else
                        ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -90), 0, 0);
                else //LEFT COLUMN
                    ((ViewGroup.MarginLayoutParams) itemViewHolder.itemLayout.getLayoutParams()).setMargins(0, dpToPx(holder.itemView.getResources(), -60), 0, 0);


                View.OnClickListener onMostVotedClickListener = optionView ->
                        ItemAdapter.this.onItemOptionClickListener.onOptionClick(
                                optionView,
                                currentItem,
                                mostVotedOption,
                                position);

                View.OnClickListener onLessVotedClickListener = optionView ->
                        ItemAdapter.this.onItemOptionClickListener.onOptionClick(
                                optionView,
                                currentItem,
                                lessVotedOption,
                                position);

                Utils.OnDoubleClickListener onDoubleClickListener = () ->
                        onItemDoubleClickListener.onDoubleClick(currentItem);

                itemViewHolder.mostVotedOption.setOnDoubleClickListener(onDoubleClickListener);
                itemViewHolder.lessVotedOption.setOnDoubleClickListener(onDoubleClickListener);
                itemViewHolder.mostVotedOption.setOnClickListener(onMostVotedClickListener);
                itemViewHolder.lessVotedOption.setOnClickListener(onLessVotedClickListener);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + getItemViewType(position));
        }

    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout itemLayout;
        MostVotedButton mostVotedOption;
        LessVotedButton lessVotedOption;
        TextView mostVotedOptDescription;
        TextView lessVotedOptDescription;
        TextView mostVotedOptPercentage;
        TextView lessVotedOptPercentage;
        ImageView mostVotedVoteIcon;
        ImageView lessVotedVoteIcon;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemLayout = (ConstraintLayout) itemView;
            mostVotedOption = itemView.findViewById(R.id.most_voted_option);
            lessVotedOption = itemView.findViewById(R.id.less_voted_option);
            mostVotedOptDescription = itemView.findViewById(R.id.most_voted_description);
            lessVotedOptDescription = itemView.findViewById(R.id.less_voted_description);
            mostVotedOptPercentage = itemView.findViewById(R.id.most_voted_percentage);
            lessVotedOptPercentage = itemView.findViewById(R.id.less_voted_percentage);
            mostVotedVoteIcon = itemView.findViewById(R.id.vote_ic_most_voted);
            lessVotedVoteIcon = itemView.findViewById(R.id.vote_ic_less_voted);
        }

    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {
        CategoryConstraintLayout categoryLayout;
        TextView categoryName;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryLayout = (CategoryConstraintLayout) itemView;
            categoryName = itemView.findViewById(R.id.category_name);
        }

    }

    private int getItemColumnInGrid(ArrayList<ListItem> listItems, int itemPosition) {
        int count = 0;
        do {
            count++;
        } while (listItems.get(itemPosition - count).getListItemType() != ListItem.TYPE_CATEGORY);

        return 1 - count % 2;

    }

}
